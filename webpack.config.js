const path = require('path');
const webpack = require('webpack');

const csso = require('postcss-csso');
const autoprefixer = require('autoprefixer');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');


// Настройки из файла package.json
const css_hash = require('./package').config.css;



// Переменная окружения
let NODE_ENV = process.env.NODE_ENV || 'development';

// Параметры для разработки
const development = require('./config/development');

// Параметры для продакшена
const production  = require('./config/production');

// Общие параметры конфига
const common = {
    context: __dirname,

    entry: './client.js',

    output: {
        filename: 'bundle.js',
        path: './app/public'
    },

    module: {
        loaders:
        [
            {
                test: /\.md$/,
                exclude: '/node_modules/',
                loader: 'markdown-loader'
            },
            {
                test: /\.(js|jsx)$/,
                exclude: '/node_modules/',
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            },

            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract(
                    'style-loader',
                    'css-loader'
                )
            },

            {
                test: /\.styl$/,
                loader: ExtractTextPlugin.extract(
                    'style-loader',
                    `css-loader?sourceMap&modules&importLoaders=1&localIdentName=${css_hash}!postcss-loader!stylus-loader`
                )
            },

            {
                test: /\.(jpe?g|png|gif|svg)$/,
                loader: 'file-loader?name=[name].[ext]'
            }
        ]
    },

    stylus: {
        import: path.resolve(__dirname, 'app/styles/importer.styl')
    },
};



// Собираем конфиг
let Config = Object.assign({}, common, development);

if (NODE_ENV === 'production') {
  console.info('Building for production');
  
  Config = Object.assign( {}, common, production );
}

module.exports = Config;