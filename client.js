import ReactDOM from 'react-dom';
import React from 'react';
import {routes} from './routes/routes.jsx';
import Redux from 'redux';
import {Provider} from 'react-redux';
import css from './app/main.styl';

function reducer(state) { return state; }

const store = Redux.createStore(reducer, window.PROPS);

ReactDOM.render(
  <Provider store={store}>
    {routes}
  </Provider>, document
);
