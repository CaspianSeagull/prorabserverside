module.exports = {
  plugins: [
    require('postcss-csso'),
    require('postcss-discard-duplicates'),
    require('autoprefixer')
  ]
}