const
  /** Домен API-сервера */
  // globalDomain = "http://92.222.71.0",
  globalDomain = 'http://api.pro-rab.ru',

  /** Ключ Google API */
  googleAPIKey = 'AIzaSyA1l_CX9YVk3O_qywJUaVT0RIiRK49AFQw',

  /** Количество объявлений на странице */
  paginationLimit = 5;

/**  Составление роутов
 * 
 * @param {string} routeString - строка роута
 * @returns {string} route
 */
function setRoute(routeString) {
  return globalDomain + routeString;
}

/** Роуты для закладок
 * @memberOf API
 */
export const Bookmarks = {
  /** Получение списка меток
   * @param {id} id  - id закладки
   */
  item: id => setRoute(`/bookmarks/${id}/`),

  /** Все закладки текущего пользователя */
  items: setRoute('/bookmarks/')
};

/** Роуты для юзеров
 * @memberOf API
 */
export const Accounts = {
  /** Получение информации о конкретном пользователе
   * @param {number} id - id пользователя
   */
  users: (id) => setRoute(`/accounts/users/${id}/`),


  /** Получаем код подтверждения */
  getPasscode: setRoute('/accounts/users/'),

  /** Информация о текущем пользователе */
  user: setRoute('/accounts/users/current/'),

  /** Верификация пользователя */
  verify: setRoute('/accounts/verify/')
};

/** Роуты для маркеров карты
 * @memberOf API
 */
export const Markers = {
  /** Получение списка меток
   * @param {string} type  - тип объявления
   * @param {array} points - массив координат полигона для поиска
   * @param {object} filter - параметры фильтрации меток
   *
   * @returns {string} Route
   */
  type: (type, filter='') => {
    // let polygon = '';
    let f_type = formatType(type);

    filter = formatFilter(filter);

    return setRoute(`/adinstances/${f_type}/markers/${filter}`);
  }
};

/** Роуты для транспорта
 * @memberOf API
 */
export const Transport = {
  /** Параметры модели */
  types: {
    categories: setRoute('/adsparameters/transport/categories/'),

    /** Список брендов транспорта
     * @param {number} category_id - id категории для фильтрации брендов
     */
    brands: (category_id) => category_id ?
      setRoute(`/adsparameters/transport/brands/?categories=${category_id}`) :
      setRoute('/adsparameters/transport/brands/'),

    /** Список моделей транспорта
     * @param {number} category_id - id категории
     * @param {number} brand_id    - id бренда
     */
    models: (category_id, brand_id) => {
      let filterString = formatFilter({
        category: category_id,
        brand:    brand_id
      });

      return setRoute(`/adsparameters/transport/model/${filterString}`);
    }
  },

  /** Получение параметров по категории транспорта
   * @param {number} type_id - id категории транспорта
   */
  params: (type_id) => type_id ?
    setRoute(`/adsparameters/transport/parameters/?categories=${type_id}`) :
    setRoute('/adsparameters/transport/parameters/')
};

/** Роуты для грузов 
 * @memberOf API
 */
export const Cargo = {};

/** Роуты для магазинов
 * @memberOf API
 */
export const Shop = {
  /** Получение категорий магазинов */
  categories: setRoute('/adsparameters/shop/categories/')
};

/** Роуты для услуг
 * @memberOf API
 */
export const Service = {
  /** Получение категорий услуг */
  categories: setRoute('/adsparameters/service/categories/')
};

/** Роуты для вакансий и резюме */
export const Job = {
  /** Категории вакансий и резюме */
  categories: setRoute('/adsparameters/job/categories/')
};

/** Роуты для оборудования
 * @memberOf API
 */
export const Equipment = {
  /** Параметры модели */
  types: {
    categories: setRoute('/adsparameters/equipment/categories/'),

    /** Список брендов оборудования
     * @param {number} category_id - id категории для фильтрации брендов
     */
    brands: (category_id) => category_id ?
      setRoute(`/adsparameters/equipment/brands/?categories=${category_id}`) :
      setRoute('/adsparameters/equipment/brands/'),

    /** Список моделей оборудования
     * @param {number} category_id - id категории
     * @param {number} brand_id    - id бренда
     */
    models: (category_id, brand_id) => {
      let filterString = formatFilter({
        category: category_id,
        brand:    brand_id
      });

      return setRoute(`/adsparameters/equipment/model/${filterString}`);
    }
  },

  /** Получение параметров оборудования по категории
   * @param {number} category_id - id категории
   */
  params: (category_id) => category_id ?
    setRoute(`/adsparameters/equipment/parameters/?categories=${category_id}`) :
    setRoute('/adsparameters/equipment/parameters/')
};

/** Роуты для работы с локациями
 * @memberOf API
 */
export const Geo = {
  /** Получение списка городов по id региона
   * @param {number} regionId - id региона
   */
  cities: (regionId) => regionId ? setRoute(`/places/cities/?region=${regionId}`) : setRoute('/places/cities/'),

  /** Список регионов */
  regions: setRoute('/places/regions/'),

  /** Список стран */
  countries: setRoute('/places/countries/')
};

/** Роуты для общей работы с объявлениями
 * @memberOf API
 */
export const Ads = {

  /** Роут для загрузки объявления */
  uploadItem: (type) => {
    let formattedType = formatType(type);
    return setRoute(`/adinstances/${formattedType}/`);
  },

  /** Получение объявлений с пагинацией и фильтром
   * @param {string} type - тип объявления
   * @param {number} page - страница
   * @param {object} filter - объект фильтра.
   * @param {object} extra - дополнительные параметры.
   *
   * @example
   * API.Ads.items('transport', 2, {category: 1, price_0: 500})
   * // 'http://92.222.71.0/adinstances/transports/?limit=5&offset=10&category=1&price_0=500'
   */
  items: (type, page, filter='', extra) => {
    type   = formatType(type);
    filter = Object.assign({}, filter, {is_active: 2});

    let limit = paginationLimit;

    let offset = paginationLimit * --page;

    if (extra.all) {
      limit  = 9999;
      offset = 0;
      delete filter.is_active;
    }


    let filterString = formatFilter(filter, true);

    return setRoute(`/adinstances/${type}/?limit=${limit}&offset=${offset}${filterString}`);
  },

  /** Получение отдельного объявления по типу
   * @param {string} type - тип объявления
   * @param {number} id   - id объявления
   */
  item: (type, id) => {
    let formattedType = formatType(type);
    return setRoute(`/adinstances/${formattedType}/${id}/`);
  },

  /** Получение диапазонов цен по типу объявления
   * @param {string} type - тип объявления
   */
  priceRange: (type) => {
    let formattedType = formatType(type);
    return setRoute(`/adinstances/${formattedType}/price_range/`);
  },

  /** Роут загрузки изображений */
  images: setRoute('/adinstances/image/')
};

/** Роуты для работы с API Google
 * @memberOf API
 */
export const Google = {

  /** Получение значений для автозаполнения
   * @param {string} string - строка для автокомплита
   */
  autoComplete: string =>
    `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${string}&key=${googleAPIKey}`,

  /** Преобразование координат в адрес
   * @param {number} lat - широта
   * @param {number} lng - долгота
   */
  coordsToAddress: (lat, lng) =>
    `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${googleAPIKey}&language=ru`
};

/** API Routes construnctor
 * @constructor
 */
const API = {
  Ads, Transport, Equipment, Cargo, Shop, Service, Job,

  Google, Geo, Markers,

  Bookmarks, Accounts
};

export default API;




/** Форматирование строки объявления
 * @param {string} type - тип объявления
 *
 * @return {string} formattedType - отформатированный тип
 */
function formatType(type) {
  let formattedType = type;

  switch (type) {
    case 'transport': formattedType = 'transports'; break;
    case 'equipment': formattedType = 'equipments'; break;
    case 'vacancy':   formattedType = 'vacancies';  break;
    case 'service':   formattedType = 'services';   break;
    case 'resume':    formattedType = 'resumes';    break;
    case 'cargo':     formattedType = 'cargoes';    break;
    case 'shop':      formattedType = 'shops';      break;
    default:          formattedType = type;         break;
  }

  return formattedType;
}

/** Форматирование строки объявления
 * @param {object} parameters - JSON объект с параметрами для фильтрации
 * @param {boolean} append - указывает, является ли строка фильтра продолжением GET-запроса
 *
 * @return {string} string - отформатированная строка фильтра
 */
function formatFilter(parameters, append=false) {
  let filterString;

  if(append) {
    filterString = '&';
  } else {
    filterString = '?';
  }

  for (let param in parameters) {
    let value = parameters[param];

    if (value !== undefined) {
      filterString += `${param}=${value}&`;
    }
  }

  filterString = filterString.slice(0,-1);

  return filterString;
}