import React from 'react';
import {Link} from 'react-router';
import axios from 'axios';
import API from '../../API';

import User from '../User/User';

import css from './style.styl';

export default class TopLine extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      setCityActive: false,
      currentRegionId: undefined,
      places: {
        regions: [],
        cities: []
      }
    }
  }

  fetchRegions() {
    axios
      .get(API.Geo.regions)
      .then( response => {
        this.setState({
          currentRegionId: response.data[0].id,
          places: {
            regions: response.data
          }
        }, () => this.fetchCities());
      }).catch(error => console.warn(error));
  }

  fetchCities() {
    axios
      .get(API.Geo.cities(this.state.currentRegionId))
      .then( response => {
        this.setState({
          places: {
            regions: this.state.places.regions,
            cities: response.data.map( city => city.name )
          }
        });
      }).catch(error => console.warn(error));
  }

  handleRegionSelect(e) {
    let currentRegionId = parseInt(e.target.value);

    this.setState({
      currentRegionId
    }, () => {
      this.fetchCities();
    });
  }

  handleCitySelect(e) {
    let city = e.target.value;

    this.props.setUserCity(city);

    localStorage.setItem('clientCity', city);

    this.setState({
      setCityActive: false
    });
  }

  componentDidMount() {
    this.fetchRegions();
  }

  render() {
    return (
      <div className={css.topline}>
        <div className="row">

          {this.renderLocation(this.props.city)}

          <div className="col-xs-12 col-sm-7 col-md-7">
            <User
              authorized={this.props.authorized}
              user={this.props.user}
              userLogout={this.props.userLogout}
              verifyUser={this.props.verifyUser}
            />
          </div>

        </div>
      </div>
    );
  }

  renderLocation(city) {
    return(
      <div className="col-xs-12 col-sm-5 col-md-5">
        <div className={css.logo}>
          <Link to="/"><img src="/img/logo.png" alt="СтройСервис"/></Link>

          {
            this.state.setCityActive ? (
              <div className={css.location}>

                <select defaultValue={this.state.currentRegionId} onChange={this.handleRegionSelect.bind(this)}>
                  <option value="">Регион</option>

                  {
                    this.state.places.regions.map(
                      region =>
                        <option
                          key={'region_' + region.id}
                          value={region.id}
                        >{region.name}</option> )
                  }
                </select>

                <br/>

                <select onChange={this.handleCitySelect.bind(this)}>
                  <option value="">Город</option>

                  {
                    this.state.places.cities.map( city => <option key={'city_' + city} value={city}>{city}</option> )
                  }
                </select>

              </div>
            ) : (
              <div className={css.location}>
                <span><b>{city}</b></span>
                <button onClick={() => this.setState({setCityActive: true})} className={css.citySelect}>Указать город</button>
              </div>
            )
          }
        </div>
      </div>
    );
  }
}