import React    from 'react';
import {Link}   from 'react-router';
import AuthForm from '../AuthForm/AuthForm';
import css      from './style.styl';

/** Компонент с описанием пользователя и его действиями
 * 
 * @export
 * @class User
 * @extends {React.Component}
 */
export default class User extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      openedMenu: false,
      timeouts: 0,
      user: undefined,
      showAuth: false
    };
  }

  showAuth() {
    this.setState({showAuth:true});
  }

  toggleActive() {
    this.setState({
      openedMenu: !this.state.openedMenu
    });
  }

  userLogOut() {
    this.props.userLogout();
  }

  checkLocalUserInfo() {
    let localUserInfo = localStorage.getItem('userInfo');

    if (localUserInfo && (localUserInfo !== null || localUserInfo !== 'undefined')) {
      let user = JSON.parse(localUserInfo);

      this.setState({user});
    }
  }

  componentDidMount() {
    window.setTimeout( this.checkLocalUserInfo.bind(this), 1000);
  }

  render() {
    return (
      <div className={css.User}>
        {
          this.props.authorized ?
            <Link
              activeClassName={css.activeLink}
              to="/catalog/bookmarks/transport"
            >Закладки</Link> : ''
        }

        {this.renderUserMenu()}
        {this.props.authorized ? this.renderSubmenu() : null}
      </div>
    );
  }

  renderUserMenu() {
    let token = localStorage.getItem('clientToken');

    if (token && token !== 'undefined' && typeof token === 'string') {
      return(
        <div className={css.userInfo}>
          {this.renderUserName()}
          {this.renderUserPhoto()}
        </div>
      );
    } else {
      return (
        <div>

          {
            this.state.showAuth
              ? <AuthForm verifyUser={this.props.verifyUser}/>
              : <span className={css.showAuth} onClick={this.showAuth.bind(this)}>Войти</span>
          }

          {
            !this.state.showAuth ? <Link className={css.newAdButton} to='/new'>Создать объявление</Link> : null
          }

        </div>

      );
    }

  }

  renderUserPhoto() {
    if (this.state.user) {
      return(
        <div className={css.photo}>
          <img src={this.state.user.avatar_thumbnail} alt=""/>
        </div>
      );
    } else {
      return(
        <div className={css.photo}>
          <img src="/img/no-photo.png" alt=""/>
        </div>
      );
    }
  }

  renderUserName() {
    let name = 'Unnamed user';

    if (this.state.user) {

      let userHasName = this.state.user.first_name && this.state.user.last_name;

      if (userHasName) {
        name = this.state.user.first_name + ' ' + this.state.user.last_name;
      } else {
        name = this.state.user.phone;
      }

    }

    return <button>{name}</button>;
  }

  renderSubmenu() {
    let token = localStorage.getItem('clientToken');

    const userAds = this.props.user.ads_count;

    let firstNotEmptyType;

    for (let category in userAds) {
      if (userAds[category] > 0) {
        firstNotEmptyType = category;
        break;
      }
    }

    if (token && typeof token === 'string') {
      return  (
        <nav>
          <div>
            <Link to={'/catalog/transport/rent'}>Каталог</Link>
            <Link to={'/new'}>Подать объявление</Link>
            <Link to={`/user/ads/${firstNotEmptyType}`}>Мои объявления</Link>
            <Link to="/user/profile">Редактировать профиль</Link>
            <a onClick={this.userLogOut.bind(this)}>Выход</a>
          </div>
        </nav>
      );
    }

  }
}