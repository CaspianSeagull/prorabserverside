import React from 'react';
import axios from 'axios';
import * as API from '../../API';

import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import * as UserActions from '../../redux/actions/UserActions';


class Geolocator extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let clientCity = localStorage.getItem('clientCity');

    if (clientCity && clientCity !== 'undefined') {
      this.props.UserActions.setUserCity(clientCity);
    } else if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition( (position) => {
        this.setUserLocation(position.coords.latitude, position.coords.longitude);
        this.detectCity(position.coords.latitude, position.coords.longitude);
      });
    } else {
      console.warn('[APP] Ваш браузер не поддерживает возможности геолокации');
    }
  }

  detectCity(lat, lng) {
    axios
      .get(API.Google.coordsToAddress(lat, lng))
      .then(response => {
        response.data.results.forEach( (address) => {
          let
            isCity      = address.types.includes('administrative_area_level_2'),
            isPolitical = address.types.includes('political');

          if (isCity && isPolitical) {
            let city = address.address_components[0].short_name;

            this.props.UserActions.setUserCity(city);
          }
        });
      }).catch(error => console.warn(error));
  }

  setUserLocation(lat, lng) {
    this.props.UserActions.setUserCoords(lat, lng);

    localStorage.setItem('userLocation', JSON.stringify({lat, lng}) );
  }

  render() {return null;}
}



export default connect(mapStateToProps, mapDispatchToProps)(Geolocator);
function mapStateToProps(state) {
  return {
    user: state.user
  };
}
function mapDispatchToProps(dispatch) {
  return {
    UserActions: bindActionCreators(UserActions, dispatch),
  };
}