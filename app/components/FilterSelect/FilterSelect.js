import React from 'react';
import css from './style.styl';

export default class FilterSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      type: 'integer',
      currentValue: '',
      values: [],
      all: [0,0]
    };
  }

  // <editor-fold desc="Данные">
  configFilterFromState() {
    let newFilterMixin = {};

    if (this.state.currentValue !== undefined) {
      newFilterMixin[this.props.param] = this.state.currentValue;
    }

    this.props.handler(newFilterMixin, this.props.isCustomParam);
  }


  initState(values, type) {
    values = values || [];
    type   = type   || 'string';

    let all = undefined;

    if (type === 'string') {
      all = undefined;
    } else {
      if (values.length > 1) {
        let min = values[0][0];
        let max = values[values.length - 1][1];

        all = [min, max];
      } else if (values.length === 1) {
        all = values[0];
      }
    }

    this.setState({type, values, all});
  }
  // </editor-fold>

  // <editor-fold desc="Хэндлеры">
  handleCheckChange(e) {
    this.setState({
      currentValue: this.props.isCustomParam ? JSON.parse(e.target.value) : e.target.value
    }, () => this.configFilterFromState());
  }
  // </editor-fold>

  // <editor-fold desc="Жизненный цикл">
  componentDidMount() {
    this.initState(this.props.values, this.props.type);
  }

  componentWillReceiveProps(nextProps) {
    this.initState(nextProps.values, nextProps.type);
  }

  render() {
    let key = 1;
    return (
      <div className={css.FilterSelect}>
        <select onChange={this.handleCheckChange.bind(this)} required>
          <option value="">{this.props.title}</option>
          <option value="">Все варианты</option>
          {
            this.state.values.map( value =>
            {
              let val = this.props.isCustomParam ? JSON.stringify(value) : value, text = value;

              if (Array.isArray(value)) {
                text = `${value[0]} — ${value[1]}`;
              }

              return <option key={`prop_${key++}`} value={val}>{text}</option>;
            })
          }
        </select>
      </div>
    );
  }
  // </editor-fold>
}
