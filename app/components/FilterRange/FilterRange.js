import React from 'react';

import InputRange from 'react-input-range';
import InputRangeStyle from './InputRange.css';

import css from './style.styl';

export default class FilterRange extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      min: 0,
      max: 1000000,
      values: {
        min: 0,
        max: 1000000
      }
    };
  }

  // <editor-fold desc="Данные">
  setRangeFromProps() {
    this.setState({
      min: this.props.min,
      max: this.props.max,
      values: {
        min: this.props.min,
        max: this.props.max,
      }
    });
  }
  configFilterFromState() {
    let newFilterMixin = {};
    newFilterMixin[this.props.paramMin] = this.state.values.min;
    newFilterMixin[this.props.paramMax] = this.state.values.max;
    console.log(newFilterMixin);
    this.props.handler(newFilterMixin);
  }
  // </editor-fold>

  // <editor-fold desc="Хэндлеры">
  handleInputPriceChange(e) {
    let
      type       = e.target.dataset.val,
      valueState = Object.assign({}, this.state.values);

    valueState[type] = parseInt(e.target.value);

    this.setState({
      values: valueState,
    });
  }
  handleValuesChange(component, values) {
    this.setState({
      values: values,
    });
  }
  handleValuesChangeComplete(component, values) {
    this.setState({
      values: values,
    }, () => this.configFilterFromState());
  }
  // </editor-fold>

  // <editor-fold desc="Жизненный цикл">
  componentDidMount() {
    this.setRangeFromProps();
  }
  render() {
    let valuesExists = this.state.min && this.state.max;
    let valuesValid  = this.state.min < this.state.max;

    if (valuesExists && valuesValid) {
      return (
        <div className={css.FilterRange}>
          <h4>{this.props.title}</h4>
          <InputRange
            maxValue={this.state.max}
            minValue={this.state.min}
            value={this.state.values}
            onChange={this.handleValuesChange.bind(this)}
            onChangeComplete={this.handleValuesChangeComplete.bind(this)}
          />
          <div className={css.rangeInputs}>
            <input data-val="min" type="number" onChange={this.handleInputPriceChange.bind(this)} value={this.state.values.min}/>
            <span>—</span>
            <input data-val="max" type="number" onChange={this.handleInputPriceChange.bind(this)} value={this.state.values.max}/>
          </div>
        </div>
      );
    } else return null;

  }
  // </editor-fold>
}
