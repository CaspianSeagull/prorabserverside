import React  from 'react';
import {Link} from 'react-router';
import css    from './style.styl';
import axios  from 'axios';

import API from '../../API';

import { connect }            from 'react-redux';

/**
 * Компонент UserAdActions
 * @constructor
 */
export class UserAdActions extends React.Component {
  constructor(props) {
    super( props );

    this.state = {
      isCurrentUser: false,
      isActive: false,
      isConfirm: false,
      deleting: false
    };
  }



  deleteItem(clientToken) {

    axios({
      url: API.Ads.item(this.props.type, this.props.id),
      method: 'DELETE',
      headers: {
        'Authorization': `Token ${clientToken}`
      }
    })
    .then( (response) => {

      if (response.status === 204) {
        this.setState({deleting: false, isConfirm: false}, () => {
          this.props.fetchAds();
        });
      }

    })
    .catch(err => console.log(err.response));

  }


  setAdActiveStatus(clientToken) {

    axios({
      url: API.Ads.item(this.props.type, this.props.id),
      method: 'PATCH',
      data: {
        is_active: this.state.isActive
      },
      headers: {
        'Authorization': `Token ${clientToken}`
      }
    })
    .then( _ => null)
    .catch(err => console.log(err.response));

  }

  confirmOn() {
    this.setState({isConfirm: true});
  }

  confirmOff() {
    this.setState({isConfirm: false});
  }

  handleDeleteClick() {
    this.setState({deleting: true}, () => {

      let clientToken = localStorage.getItem('clientToken');

      if (clientToken && clientToken !== 'undefined') {
        this.deleteItem(clientToken);
      }

    });
  }

  editItem() {
    console.info(`[APP] Editing ${this.props.type} item #${this.props.id}`);
  }

  switchActive() {
    this.setState({
      isActive: !this.state.isActive
    }, () => {
      let clientToken = localStorage.getItem('clientToken');

      if (clientToken && clientToken !== 'undefined') {
        this.setAdActiveStatus(clientToken);
      }
    });
  }


  handleProps(props) {

    if (props.user.fetching === 'ready') {
      let isCurrentUser = props.user.info.id === props.ownerId;

      this.setState({
        isActive: props.isActive,
        isCurrentUser
      });
    }

  }

  //region Lifecycle
  componentDidMount() {
    this.handleProps(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.handleProps(nextProps);
  }
  //endregion

  //region Render
  render() {

    if (this.state.isCurrentUser) {
      return (
        <div className={css.userAdActions}>

          {this.renderConfrimTooltip()}

          <div onClick={this.switchActive.bind(this)} className={`${css.switch} ${this.state.isActive ? css.active : ''}`}></div>
          <Link to={`/edit/${this.props.type}/${this.props.id}`} className={css.edit}></Link>
          <div onClick={this.confirmOn.bind(this)} className={css.delete}></div>
        </div>
      );
    }

    else return null;

  }


  renderConfrimTooltip() {
    if (this.state.deleting) {
      return(
        <div className={css.confirm}>
          <span>Удаление объявления...</span>
        </div>
      );
    } else if (this.state.isConfirm) {
      return (
        <div className={css.confirm}>
          <span>Это действие нельзя отменить. Вы уверены, что хотите удалить объявление?</span>
          <button onClick={this.handleDeleteClick.bind(this)}>Да</button>
          <button onClick={this.confirmOff.bind(this)}>Нет</button>
        </div>
      );
    } else return null;
  }
  //endregion
}

export default connect(mapStateToProps)(UserAdActions);
function mapStateToProps(state) {
  return {
    user: state.user
  };
}