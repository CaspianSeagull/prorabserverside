import React from 'react';

import css from './style.styl';

export class Placemark extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={css.Placemark}>
      </div>
    );
  }
}



export class MapCluster extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={css.Cluster}>
        <span>
          {this.props.amount}
        </span>
      </div>
    );
  }
}
