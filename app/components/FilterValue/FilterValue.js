import React from 'react';
import css from './style.styl';

export default class FilterValue extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      type: 'number',
      value: ''
    };
  }

  // <editor-fold desc="Данные">
  configFilterFromState() {
    let newFilterMixin = {};
    newFilterMixin[this.props.param] = this.state.value;
    console.log(newFilterMixin);
    this.props.handler(newFilterMixin);
  }
  // </editor-fold>

  // <editor-fold desc="Хэндлеры">
  handleInputChange(e) {
    let value = e.target.value;

    if (this.state.type === 'number') {
      value = parseInt(value);
    }

    this.setState({value}, () => this.configFilterFromState());
  }
  // </editor-fold>

  // <editor-fold desc="Жизненный цикл">
  componentDidMount() {
    let type = this.props.type;
    let initValue = this.props.initValue || '';

    this.setState({type, initValue});
  }
  render() {
    return (
      <div className={css.FilterValue}>
        <h4>{this.props.title}</h4>

        <input
          type={this.state.type}
          value={this.state.value}
          placeholder={this.props.title}
          onChange={this.handleInputChange.bind(this)}
        />
      </div>
    );
  }
  // </editor-fold>
}
