export const MenuList = {
  transport: {
    id: 1,
    develop: false,
    title: 'Спецтехника',
    subroutes: [
      {title: 'Аренда',            route: '/catalog/transport/rent'},
      {title: 'Купля/Продажа',     route: '/catalog/transport/sell'},
      // {title: "Запчасти и сервис", route: "/catalog/transport/service"}
    ]
  },

  equipment: {
    id: 2,
    develop: false,
    title: 'Строительноe оборудование',
    subroutes: [
      {title: 'Аренда',  route: '/catalog/equipment/rent'},
      {title: 'Продажа', route: '/catalog/equipment/sell'}
    ]
  },

  cargoes: {
    id: 3,
    develop: false,
    title: 'Грузы и грузоперевозки',
    subroutes: [
      {title: 'Все грузы',        route: '/catalog/cargoes'},
      //{title: "Рассчет маршрута", route: "/catalog/cargoes/routes"},
      //{title: "Поиск грузов",     route: "/catalog/cargoes/search"},
      //{title: "Перевозка",        route: "/catalog/cargoes/logistic"}
    ]
  },

  shops: {
    id: 4,
    develop: false,
    title: 'Магазины и стройматериалы',
    subroutes: [
      {title: 'Все магазины', route: '/catalog/shops'},
    ]
  },

  service: {
    id: 5,
    develop: false,
    title: 'Услуги',
    subroutes: [
      {title: 'Заказы',       route: '/catalog/service/order'},
      {title: 'Предложения',  route: '/catalog/service/offer'}
    ]
  },

  vacancy: {
    id: 6,
    develop: false,
    title: 'Работа',
    subroutes: [
      {title: 'Вакансии', route: '/catalog/vacancy/all'},
      {title: 'Резюме',   route: '/catalog/resume/all'},
    ]
  },

  resume: {
    id: 7,
    hidden: true,
    develop: false,
    title: 'Работа',
    subroutes: [
      {title: 'Вакансии', route: '/catalog/vacancy/all'},
      {title: 'Резюме',   route: '/catalog/resume/all'},
    ]
  }
};