import React  from 'react';
import {Link} from 'react-router';

import {MenuList} from './menulist';
import css from './style.styl';


export default class Menu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      titleMobile: 'Спецтехника'
    };
  }

  setCurrentMobileMenu(e) {
    let titleMobile = e.target.dataset.title;
    this.refs.mobileMenuCheck.checked = false;
    this.setState({titleMobile});
  }


  componentDidMount() {
    window.addEventListener('scroll', () => {
      if (window.innerWidth > 767) {
        if (window.pageYOffset > 82) {
          this.refs.container.style.width = this.refs.container.clientWidth + 'px';
          this.refs.container.classList.add(css.fixed);
        } else {
          this.refs.container.style.width = '100%';
          this.refs.container.classList.remove(css.fixed);
        }
      }
    });
  }
  render() {
    return (
      <div ref="container" className={`${css.Menu} ${this.props.inactive ? css.inactive : ''}`}>

        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              {this.renderMainNavigation()}
            </div>
          </div>
        </div>

        {this.props.isHidden ? '' : this.renderSubNavigation()}
      </div>
    );
  }


  renderMainNavigation() {
    let categories = [];

    for (let cat in MenuList) {
      categories.push( MenuList[cat] );
    }

    return(
      <nav>
        <label htmlFor="mainmenu">{this.state.titleMobile}</label>
        <input ref="mobileMenuCheck" id="mainmenu" type="checkbox"/>
        {
          categories.map(
            item => {
              let activeClass = item.subroutes[0].route.search(this.props.currentMenu) !== -1;

              return !item.hidden ? 
                <Link
                  className={`${activeClass ? css.activeLink : ''}`}
                  key={`menu_category_${item.id}`}
                  activeClassName={css.activeLink}
                  to={item.develop ? null : item.subroutes[0].route}
                  title={item.develop ? 'Раздел в разработке' : ''}
                  data-title={item.title}
                  onClick={this.setCurrentMobileMenu.bind(this)}
                >
                  {item.title}
                </Link>
              :'';
            }
          )
        }
      </nav>
    );
  }
  renderSubNavigation() {
    return(
      <aside>
        <div className="container">
          <div className="row">

            <div className="col-xs-12 col-sm-6 col-md-8 col-lg-9">
              <div className={css.links}>
                {
                  MenuList[this.props.currentMenu] ? MenuList[this.props.currentMenu].subroutes.map(
                    item => {
                      let activeClass = false;

                      if (item.title === 'Вакансии' || item.title === 'Резюме') {
                        let translate = {
                          'Вакансии': 'resume',
                          'Резюме': 'vacancy'
                        };

                        activeClass = translate[item.title] === this.props.currentMenu;

                        console.log(this.props.currentMenu, translate[item.title], activeClass);
                      } else {
                        activeClass = item.route.search(this.props.currentType) !== -1;
                      }

                      return (
                        <Link
                          className={`${activeClass ? css.activeLink : ''}`}
                          key={`menu_action_${this.props.currentMenu}-${item.title}`}
                          activeClassName={css.activeLink}
                          to={item.route}
                        >
                          {item.title}
                        </Link>
                      );
                    }
                  ) : ''
                }
              </div>
            </div>

            <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3">
              <a href="/new">
                <div className={css.newAd}>
                  <span>Добавить объявление</span>
                  <img src="/img/icons/plus.svg" alt=""/>
                </div>
              </a>
            </div>

          </div>
        </div>
      </aside>
    );
  }
}