import React from 'react';
import axios from 'axios';

import * as API from '../../API';

export default class Address extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      address: 'Загрузка адреса',
      data: null
    };
  }

  setLocationData(location) {
    let
      lat = location.lat,
      lng = location.lon;

    axios
      .get(API.Google.coordsToAddress(lat, lng))
      .then(response => {
        this.setState({data: response.data}, () => {
          this.setAddress();
        });
      })
      .catch(error => console.warn(error));

  }
  setCity() {
    this.state.data.results.forEach( (address) => {
      let
        isCity      = address.types.includes('administrative_area_level_2'),
        isPolitical = address.types.includes('political');

      if (isCity && isPolitical) {
        address = address.address_components[0].short_name;

        this.setState({address});
      } else {
        this.setState({address: 'Не удалось определить адрес'});
      }
    });
  }
  setRegion() {
    this.state.data.results.forEach( (address) => {
      let
        isCity      = address.types.includes('administrative_area_level_2'),
        isPolitical = address.types.includes('political');

      if (isCity && isPolitical) {
        address = address.address_components[0].short_name;

        this.setState({address});
      } else {
        this.setState({address: 'Не удалось определить адрес'});
      }
    });
  }
  setAddress() {
    switch (this.props.type) {
      case 'city':
        this.setCity();
        break;

      case 'region':
        this.setRegion();
        break;

      default:
        this.setCity();
    }
  }


  componentDidMount() {
    this.setLocationData(this.props.location);
  }
  render() {
    return (
      <address>{this.state.address}</address>
    );
  }
}
