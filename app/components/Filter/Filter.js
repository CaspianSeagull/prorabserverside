import React from 'react';
import axios from 'axios';

import * as API from '../../API';

import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';
import * as AdsActions        from '../../redux/actions/AdsActions';

import FilterRange   from '../FilterRange/FilterRange';
import FilterValue   from '../FilterValue/FilterValue';
import FilterBoolean from '../FilterBoolean/FilterBoolean';
import FilterSelect  from '../FilterSelect/FilterSelect';

import css from './style.styl';

class Filter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentCategory: 'transport',
      currentCategoryId: '',
      categoryTypes: [],
      currentRegionId: undefined,
      parameters: [],
      currentFilter: {},
      priceRanges: {},
      custormParamsFilter: {},
      customParamsOpened: false,
      regionReady: false,
      places: {
        regions: [],
        cities: []
      }
    };
  }

  // <editor-fold desc="Фетчинг">
  fetchAdsData(filterParams) {
    this.props.AdsActions.setAdsListAsync(
      this.state.currentCategory,
      1,
      filterParams || this.props.ads.filterParams
    );
  }
  fetchPriceRange() {
    let category = this.state.currentCategory;

    switch(category) {
      case 'transport':
      case 'transports':
      case 'equipment':
      case 'equipments':
      case 'cargo':
      case 'cargoes':
      case 'service':
      case 'services':
        axios
          .get(API.Ads.priceRange(category))
          .then( response => {
            this.setState({priceRanges: response.data});
          })
          .catch(error => console.warn(error));
        break;

      default:
        console.warn('У категории нет ранжирования по цене');
    }
  }
  fetchCustomParameters() {
    switch(this.state.currentCategory) {
      case 'transport':
        axios
          .get(API.Transport.params(this.state.currentCategoryId))
          .then( response => {
            this.setState({parameters: response.data});
          })
          .catch(error => console.warn(error));
        break;

      case 'equipment':
        axios
          .get(API.Equipment.params(this.state.currentCategoryId))
          .then( response => {
            this.setState({parameters: response.data});
          })
          .catch(error => console.warn(error));
        break;

      default: break;
    }
  }
  fetchRegions() {
    axios
      .get(API.Geo.regions)
      .then( response => {
        this.setState({
          currentRegionId: response.data[0].id,
          places: {
            regions: response.data
          }
        }, () => this.fetchCities());
      }).catch(error => console.warn(error));
  }
  fetchCities() {
    axios
      .get(API.Geo.cities(this.state.currentRegionId))
      .then( response => {
        this.setState({
          places: {
            regions: this.state.places.regions,
            cities: response.data.map( city => city.name )
          }
        });
      }).catch(error => console.warn(error));
  }
  fetchCategoryTypes() {
    switch(this.state.currentCategory) {
      case 'transport':
        axios
          .get(API.Transport.types.categories)
          .then(response => {
            this.setState({
              categoryTypes: response.data
            });
          });
        break;

      case 'equipment':
        axios
          .get(API.Equipment.types.categories)
          .then(response => {
            this.setState({
              categoryTypes: response.data
            });
          });
        break;

      case 'shops':
        axios
          .get(API.Shop.categories)
          .then(response => {
            this.setState({
              categoryTypes: response.data
            });
          });
        break;

      case 'service':
        axios
          .get(API.Service.categories)
          .then(response => {
            this.setState({
              categoryTypes: response.data
            });
          });
        break;

      case 'vacancy':
      case 'resume':
        axios
          .get(API.Job.categories)
          .then(response => {
            this.setState({
              categoryTypes: response.data
            });
          });
        break;

      default:
        this.setState({categoryTypes: []});
        break;
    }
  }
  // </editor-fold>

  // <editor-fold desc="Хэндлеры">
  handleRegionSelect(e) {
    let currentRegionId = parseInt(e.target.value);

    let regionName = e.target.options[e.target.options.selectedIndex].textContent;

    if (currentRegionId) {
      this.globalFilterExtender({address: regionName}, false);

      this.setState({
        currentRegionId,
        regionReady: true
      }, () => this.fetchCities());
    } else {
      this.globalFilterExtender({address: undefined}, false);
      this.setState({regionReady: false});
    }
  }
  handleCategoryTypeChange(e) {
    this.props.AdsActions.setAdsFetch('fetching');

    let type_id = e.target.value;
    let currentFilter = this.props.ads.filterParams || {};

    const newFilter = Object.assign({}, currentFilter, {
      category: type_id || ''
    });

    this.props.AdsActions.setFilterParams(newFilter);

    this.fetchAdsData(newFilter);
  }

  // </editor-fold>

  // <editor-fold desc="Работа с данными">
  submitFilter() {
    let filter = clearFilter(this.state.currentFilter);

    this.fetchAdsData(filter);
  }
  globalFilterExtender(filterMixin, isCustom) {
    let currentFilter = this.props.ads.filterParams || {};
    let currentCustomParamsFilter = this.state.custormParamsFilter;
    let newFilter = {};

    if(isCustom) {
      let custormParamsFilter = Object.assign({}, currentCustomParamsFilter, filterMixin);
      this.setState({custormParamsFilter});

      newFilter = Object.assign({}, currentFilter, {parameters: JSON.stringify(custormParamsFilter)});
    } else {
      newFilter = Object.assign({}, currentFilter, filterMixin);
    }

    newFilter = clearFilter(newFilter);

    this.props.AdsActions.setFilterParams(newFilter);
    this.setState({currentFilter: newFilter});
  }
  switchCustomParam() {
    this.setState({
      customParamsOpened: !this.state.customParamsOpened
    });
  }
  // </editor-fold>

  // <editor-fold desc="Жизненный цикл компонента">
  componentDidMount() {
    let currentFilter = this.props.ads.filterParams;

    this.fetchRegions();

    this.setState({
      currentFilter,
      currentCategory: this.props.category
    }, () => {
      this.fetchCategoryTypes();
      this.fetchPriceRange();
      this.fetchCustomParameters();
    });
  }
  componentWillReceiveProps(nextProps) {
    if (this.state.currentCategory !== nextProps.category) {
      this.setState({
        currentCategory: nextProps.category
      }, () => {
        this.fetchCategoryTypes();
        this.fetchPriceRange();
        this.fetchPriceRange();
      });
    }

    if(this.state.currentCategoryId !== nextProps.ads.filterParams.category) {

      console.info('Смена категории на' + nextProps.ads.filterParams.category);

      this.setState({
        currentCategory: nextProps.category,
        currentCategoryId: nextProps.ads.filterParams.category
      }, () => {
        this.fetchCategoryTypes();
        this.fetchPriceRange();
        this.fetchCustomParameters();
      });
    }
  }
  render() {
    let renderFilterGroup = null;
    let allCatTxt = 'Все категории';

    switch(this.state.currentCategory){
      case 'transport': renderFilterGroup = this.renderTransportFilters.bind(this); allCatTxt = 'Весь транспорт'; break;
      case 'equipment': renderFilterGroup = this.renderTransportFilters.bind(this); allCatTxt = 'Все оборудование'; break;
      case 'cargoes':   renderFilterGroup = this.renderCargoesFilters.bind(this);   allCatTxt = 'Все грузы'; break;
      case 'shops':     renderFilterGroup = this.renderShopsFilters.bind(this);     allCatTxt = 'Все магазины'; break;
      case 'service':   renderFilterGroup = this.renderServiceFilters.bind(this);   allCatTxt = 'Все услуги'; break;
      case 'vacancy':   renderFilterGroup = this.renderJobFilters.bind(this);       allCatTxt = 'Все вакансии'; break;
      case 'resume':    renderFilterGroup = this.renderJobFilters.bind(this);       allCatTxt = 'Все резюме'; break;

      default: break;
    }

    return (
      <div className={css.Filter}>
        {
          this.state.categoryTypes.length ? 
            <select onChange={this.handleCategoryTypeChange.bind(this)} name="filter_category" id="filter_category">
              <option value="">{allCatTxt}</option>
              {
                this.state.categoryTypes.map( type => {
                  return <option key={`type_${type.id}`} value={type.id}>{type.name}</option>;
                })
              }
            </select>
           : null
        }

        {renderFilterGroup()}

        <button onClick={this.submitFilter.bind(this)}>
          Фильтровать
        </button>
      </div>
    );
  }
  // </editor-fold>

  // <editor-fold desc="Рендер частей компонента">
  renderTransportFilters() {
    return (
      <div>
        <div className={css.FilterParamGroup}>
          {
            this.state.priceRanges.hasOwnProperty('price_per_hour') ? 
              <FilterRange
                title="Цена за час"
                paramMin="price_per_hour_0" paramMax="price_per_hour_1"
                min={this.state.priceRanges.price_per_hour.min}
                max={this.state.priceRanges.price_per_hour.max}
                handler={this.globalFilterExtender.bind(this)}
              />
             : ''
          }
        </div>

        {this.renderPlaceFilter()}

        {
          this.state.currentCategoryId ? (
            <div className={css.CustomFilters}>
              <button onClick={this.switchCustomParam.bind(this)}>
                Параметры поиска
                <span className={this.state.customParamsOpened ? css.opened : ''}>+</span>
              </button>
              {
                this.state.customParamsOpened
                  ? this.state.parameters.map( parameter => this.renderCustomParameter(parameter))
                  : ''
              }
            </div>
          ) : null
        }

      </div>
    );
  }
  renderJobFilters() {
    return(
      <div>
        <div className={css.FilterParamGroup}>
          <FilterRange
            title="Зарплата"
            paramMin="wages_0" paramMax="wages_1"
            min={0} max={100000}
            handler={this.globalFilterExtender.bind(this)}
          />
        </div>
      </div>
    );
  }
  renderServiceFilters() {
    return(
      <div>
        <div className={css.FilterParamGroup}>
          {
            this.state.priceRanges.hasOwnProperty('price') ? 
              <FilterRange
                title="Цена"
                paramMin="price_0" paramMax="price_1"
                min={this.state.priceRanges.price.min}
                max={this.state.priceRanges.price.max}
                handler={this.globalFilterExtender.bind(this)}
              />
            :''
          }
        </div>
      </div>
    );
  }
  renderCargoesFilters() {
    return(
      <div>
        <div className={css.FilterParamGroup}>
          {
            this.state.priceRanges.hasOwnProperty('price') ? 
              <FilterRange
                title="Цена"
                paramMin="price_0" paramMax="price_1"
                min={this.state.priceRanges.price.min}
                max={this.state.priceRanges.price.max}
                handler={this.globalFilterExtender.bind(this)}
              />
            :''
          }
        </div>

        <div className={css.FilterParamGroup}>
          <FilterRange
            title="Вес"
            paramMin="weight_0" paramMax="weight_1"
            min={0}
            max={10000}
            handler={this.globalFilterExtender.bind(this)}
          />
        </div>

        <div className={css.FilterParamGroup}>
          <FilterValue
            title="Откуда" param="address"
            type="text"
            handler={this.globalFilterExtender.bind(this)}
          />
        </div>

        <div className={css.FilterParamGroup}>
          <FilterValue
            title="Куда" param="to_address"
            type="text"
            handler={this.globalFilterExtender.bind(this)}
          />
        </div>
      </div>
    );
  }
  renderShopsFilters() {
    return null;
  }
  renderCustomParameter(param) {
    if (param.type === 'boolean') {
      return(
        <FilterBoolean
          isCustomParam={true}
          key={`param_${param.slug}`}
          title={param.name} param={param.slug}
          checked={false}
          handler={this.globalFilterExtender.bind(this)}
        />
      );
    } else {
      return(
        <FilterSelect
          isCustomParam={true}
          key={`param_${param.slug}`}
          title={param.name} param={param.slug}
          values={param.values}
          type={param.type}
          handler={this.globalFilterExtender.bind(this)}
        />
      );
    }

  }
  renderPlaceFilter() {
    return(
      <div className={css.FilterParamGroup}>
        {this.renderRegionsFilter()}
        {this.state.regionReady ? this.renderCitiesFilter() : null}
      </div>
    );
  }
  renderRegionsFilter() {
    let regions = this.state.places.regions;

    if(regions && regions.length) {
      return (
        <select onChange={this.handleRegionSelect.bind(this)}>
          <option value="">Любой регион</option>
          {
            regions.map( region => <option key={'region_' + region.id} value={region.id}>{region.name}</option>)
          }
        </select>
      );
    }
  }
  renderCitiesFilter() {
    let cities = this.state.places.cities;

    if(cities && cities.length) {
      return (
        <FilterSelect
          title="Выберите город" param="address"
          values={this.state.places.cities}
          type="string"
          handler={this.globalFilterExtender.bind(this)}
        />
      );
    }
  }
  // </editor-fold>
}

const clearFilter = (filter) => {

  for (let key in filter) {
    if (!filter[key]) {
      delete filter[key]
    }
  }

  console.log(filter);

  return filter;
}

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
function mapStateToProps(state) {
  return {
    ads: state.ads
  };
}
function mapDispatchToProps(dispatch) {
  return {
    AdsActions: bindActionCreators(AdsActions, dispatch)
  };
}