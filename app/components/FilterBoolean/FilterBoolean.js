import React from 'react';
import css from './style.styl';

export default class FilterBoolean extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      checked: false
    };
  }

  // <editor-fold desc="Данные">
  configFilterFromState() {
    let newFilterMixin = {};
    newFilterMixin[this.props.param] = this.state.checked;

    this.props.handler(newFilterMixin);
  }
  // </editor-fold>

  // <editor-fold desc="Хэндлеры">
  handleCheckChange(e) {
    this.setState({
      checked: !this.state.checked
    }, () => this.configFilterFromState());
  }
  // </editor-fold>

  // <editor-fold desc="Жизненный цикл">
  componentDidMount() {
    let checked = this.props.checked || '';

    this.setState({checked});
  }
  render() {
    return (
      <div className={css.FilterBoolean}>
        <input
          id={`filter_${this.props.param}`}
          checked={this.state.checked}
          type="checkbox"
          onChange={this.handleCheckChange.bind(this)}
        />
        <label htmlFor={`filter_${this.props.param}`} />
        <span>{this.props.title}</span>
      </div>
    );
  }
  // </editor-fold>
}
