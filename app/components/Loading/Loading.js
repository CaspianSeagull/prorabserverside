import React from 'react';

import css from './style.styl';

export default class Loading extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={css.Loading} />
    );
  }
}
