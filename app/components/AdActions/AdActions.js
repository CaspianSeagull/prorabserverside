import React  from 'react';
import {Link} from 'react-router';
import css    from './style.styl';

/**
 * Компонент с описание действий владельца объявления
 * 
 * @export
 * @class AdActions
 * @extends {React.Component}
 */
export default class AdActions extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={css.AdActions}>
        <h5>Сдавай в аренду эффективнее</h5>
        <nav>
          <Link title="В разработке" className={css.inactive}>
            <i><img src="/img/icons/vip.svg" alt=""/></i>
            <span>Получить VIP</span>
          </Link>

          <Link title="В разработке" className={css.inactive}>
            <i><img src="/img/icons/mark.svg" alt=""/></i>
            <span>Выделить предложение</span>
          </Link>

          <Link title="В разработке" className={css.inactive}>
            <i><img src="/img/icons/top.svg" alt=""/></i>
            <span>Поднять в топ</span>
          </Link>

          <Link title="В разработке" className={css.inactive}>
            <i><img src="/img/icons/special.svg" alt=""/></i>
            <span>Спецпредложение</span>
          </Link>
        </nav>
      </div>
    );
  }
}
