import React  from 'react';
import {Link} from 'react-router';

import css from './style.styl';

export default class Pagination extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      limit: 5,
      pagesCount: 0
    };
  }


  createPagesList() {
    let count = Math.floor(this.props.count / this.state.limit);

    this.props.count % this.state.limit === 0 ? '' : count++;

    let pagesList = [];

    for (let num = 1; num <= count; num++) {
      let link = num > 1 ? `${this.props.route}/${num}` : `${this.props.route}`;

      pagesList.push({num, link});
    }

    return pagesList;
  }


  render() {
    let pagesList = this.createPagesList();


    return (
      <nav className={css.Pagination}>
      {
        pagesList.length > 1 ? pagesList.map(
          page => <Link onClick={() => window.scrollTo(0,0)} activeClassName={css.activeLink} key={`page_${page.num}`} to={page.link}>{page.num}</Link>
        ) : ''
      }
      </nav>
    );
  }
}
