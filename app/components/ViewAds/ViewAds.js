import React from 'react';
import axios from 'axios';

import API from '../../API';

import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';
import Loading     from '../Loading/Loading';

import {RusShortGenitive, RusShort} from '../../types_local';

import css from './style.styl';

export default class ViewTransport extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      address: null,
      owner: null,
      showOwnerPhone: false,
      categoryParameters: [],
      paramsLoading: true
    };
  }

  /** Получаем заголовок объявления
   *
   * @param {object} adItem  - объект объявления
   * @returns {string} title - заголовок объявления
   */
  getTitle(adItem) {
    let title;

    switch(this.props.category) {
      case 'transport':
      case 'equipment':
        let
          category = adItem.category ? adItem.category.name + ' ' : '',
          brand    = adItem.brand    ? adItem.brand.name    + ' ' : '',
          model    = adItem.model    ? adItem.model    + ' ' : '';

        title = category + brand + model;
        break;

      default:
        title = adItem.title || adItem.name;
        break;
    }

    return title;
  }


  getBreadcrumbLinks() {
    let links = [];
    let category = this.props.category;

    let url = '/catalog/transport/rent';

    let text = RusShort[category];
    let linkTextAdditional = RusShortGenitive[category].toLowerCase();

    let offer_type = this.props.item.offer_type || this.props.item.type;

    if (offer_type) {
      url = `/catalog/${category}/${offer_type}`;
    } else {
      url = `/catalog/${category}`;
    }

    switch(offer_type) {
      case 'rent':
        text = `Аренда ${linkTextAdditional}`;
        break;

      case 'sell':
        text = `Продажа ${linkTextAdditional}`;
        break;

      case 'offer':
        text = `Предложения ${linkTextAdditional}`;
        break;

      case 'order':
        text = `Поиск ${linkTextAdditional}`;
        break;

      default: text = RusShort[category]; break;
    }

    links.push({url, text});

    return links;
  }

  addressDecode(lat, lng) {
    axios
      .get(API.Google.coordsToAddress(lat, lng))
      .then(
        (response) => {
          let address = response.data.results.length
            ? response.data.results[0].formatted_address
            : 'Не удалось определить адрес';

          this.setState({address});
        }
      )
      .catch( error => console.warn(error) );
  }
  getOwnerInfo(ownerID) {
    axios
      .get(API.Accounts.users(ownerID))
      .then( response => this.setState({owner: response.data}) )
      .catch( error => console.warn(error) );
  }
  showOwnerPhone(e) {
    e.preventDefault();
    this.setState({showOwnerPhone: true});
  }
  fetchParametersList(category) {
    switch(category) {
      case 'transport':
        axios
          .get(API.Transport.params())
          .then( response => this.setState({categoryParameters: response.data, paramsLoading: false}) )
          .catch( error => console.warn(error) );
        break;

      case 'equipment':
        axios
          .get(API.Equipment.params)
          .then( response => this.setState({categoryParameters: response.data, paramsLoading: false}) )
          .catch( error => console.warn(error) );
        break;

      default:
        this.setState({categoryParameters: null, paramsLoading: false});
    }
  }


  componentDidMount() {
    if (this.props.item){
      this.props.item.location ? this.addressDecode(this.props.item.location.lat, this.props.item.location.lon) : '';
      this.getOwnerInfo(this.props.item.owner);
      this.fetchParametersList(this.props.category);
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.item){
      nextProps.item.location ? this.addressDecode(nextProps.item.location.lat, nextProps.item.location.lon) : '';
      this.getOwnerInfo(nextProps.item.owner);
      this.fetchParametersList(nextProps.category);
    }
  }
  render() {
    return (
      <div className={css.ViewTransport}>

        <div className={css.top}>
          <Breadcrumbs links={this.getBreadcrumbLinks()} current={this.getTitle(this.props.item)}/>

          {/*<button className={css.bookmark}>*/}
            {/*<span>Добавить в закладки</span>*/}
          {/*</button>*/}
        </div>

        <div className={css.content}>
          <div className="row">

            <div className="col-md-8">
              {this.renderTitle(this.props.item)}
              {this.renderPrice(this.props.item, this.props.category)}
              {this.renderOwner()}
              {this.renderParameters(this.props.item.parameters, this.props.category)}
              {this.renderDescription(this.props.item, this.props.category)}
            </div>

            <div className="col-md-4">
              {this.renderPhotos(this.props.item)}
            </div>

          </div>
        </div>

      </div>
    );
  }


  renderPhotos(item) {
    if (item && item.images) {
      return(
        <div className={css.photo}>
          {
            item.images.length
              ? item.images.map(img => <img key={`photo_${img.id}`} src={img.thumbnail} alt=""/>)
              : <img src="/img/no-photo.png" alt=""/>
          }
        </div>
      );
    }

    else {
      return (
        <div className={css.photo}>
          <img src="/img/no-photo.png" alt=""/>
        </div>
      );
    }
  }
  renderTitle(item) {
    let title = this.getTitle(item);

    return(
      <div className={css.info}>
        <h1>{title}</h1>
        {item.location ? <address>{this.state.address}</address> : ''}
      </div>
    );
  }
  renderPrice(item, category) {
    let type = item.offer_type;
    let isWages = item.wages !== undefined;

    const getSellPrice = () =>
      <div className={css.price}>
        <span>цена: <b>{item.price} р</b> </span>
      </div>;

    const getRentPrice = () =>
      <div>
        <div className={css.price}>
          <span><b>{parseInt(item.price_per_hour)} ₽</b> / час работы </span>
          <span><b>{parseInt(item.price_per_work_shift)} ₽</b> / за смену </span>
        </div>

        {this.paramGroup('Мин. время работы', `${item.minimum_rental_period} ч.`)}
      </div>;

    const getWages = () =>
      <div className={css.price}>
        <span>Зарплата: <b>{item.wages} р</b> </span>
      </div>;

    if (category !== 'shop') {

      return(
        <div className={css.priceGroup}>
          { type === 'rent' ? getRentPrice() : null }
          { type === 'sell' ? getSellPrice() : null }
          { isWages ? getWages() : null }
        </div>
      );
    }
  }
  renderOwner() {
    return(
      <div className={css.owner}>
        {
          this.state.owner
            ? this.paramGroup('Контактнное лицо', `${this.state.owner.first_name} ${this.state.owner.last_name}`)
            : <Loading />
        }
        {
          this.state.showOwnerPhone ? 
            <a href={`tel:${this.state.owner.phone}`} className={css.ownerPhone}>{this.state.owner.phone}</a>
           : 
            <button onClick={this.showOwnerPhone.bind(this)} className={css.ownerPhone}>Показать телефон</button>
          
        }
      </div>
    );
  }
  renderParameters(parameters, category) {

    if (category === 'transport' || category === 'equipment') {
      return(
        <div className={css.params}>
          <h3>Параметры</h3>
          <ul className={css.parameters}>
            {
              parameters.map(
                param => {
                  let value = param.value;

                  if (param.type === 'boolean') {
                    value = param.value ? 'Да' : 'Нет';
                  }

                  if (param.measure) {
                    value += ` ${param.measure}`;
                  }

                  return <li key={`parameter_${param.slug}`}><span>{param.name}:</span><b>{value}</b></li>
                }
              )
            }
          </ul>
        </div>
      );
    }

  }
  renderDescription(item, category) {
    if(category !== 'transport' && category !== 'equipment') {
      return (
        <div className={css.description}>
          {this.paramGroup('Описание от владельца', item.description)}
        </div>
      );
    }
  }


  paramGroup(title, data) {
    return(
      <div className={css.paramGroup}>
        <label>{title}</label>
        <span>{data}</span>
      </div>
    );
  }
}
