import React from 'react';
import css from './style.styl';

/**
 * Скачивание приложения при входе через телефон
 */
export default class MobileAppSplash extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showMobilePromo: false
    };
  }

  /**
   * Прячем всплывающее окно и записываем в Local Storage,
   * что пользователь посмотрел предложение
   */
  hideMobilePromo() {
    localStorage.setItem('didUserSawPromo', true);
    this.setState({showMobilePromo: false});
  }

  // <editor-fold desc="Жизненный цикл">
  componentDidMount() {
    let didUserSawPromo = localStorage.getItem('didUserSawPromo');
    
    // Если в localStorage нет записи о посещении, показываем всплывающее окно и создаем ключ
    if (didUserSawPromo === null) {
      this.setState({showMobilePromo: true});
      localStorage.setItem('didUserSawPromo', true);
    }

    // Если пользователь уже смотел предложение, прячем его
    else if (didUserSawPromo && didUserSawPromo !== null) {
      this.setState({showMobilePromo: false});
    }

    // Иначе - показываем
    else if (!didUserSawPromo && didUserSawPromo !== null) {
      this.setState({showMobilePromo: true});
    }
  }

  render() {
    if (this.state.showMobilePromo && window.innerWidth < 640) {
      return (
        <div className={css.MobileAppSplash}>
          <h2>Часть пользуетесь телефоном?<br />У нас есть мобильное приложение!</h2>

          <div className={css.MobileLinks}>
            <a href=""><img src="/img/main-button-as.svg" alt=""/></a>
            <a href=""><img src="/img/main-button-gp.svg" alt=""/></a>
          </div>

          <span className={css.close} onClick={this.hideMobilePromo.bind(this)}>
            Перейти на сайт
          </span>
        </div>
      );
    } else return null;
  }
  // </editor-fold>
}