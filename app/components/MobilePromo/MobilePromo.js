import React from 'react';
import css   from './style.styl';

export default class MobilePromo extends React.Component {

  render() {
    return(
      <aside className={css.promo}>

        <div className={css.promo__info}>
          <h3>
            Все строительные<br />
            услуги в твоем<br />
            телефоне!
          </h3>

          <span>
              Получи бонус<br />
              за установку
            </span>
        </div>

        <div className={css.promo__links}>
          <a href="">
            <img src="/img/icons/mobile-app-as-text.svg" alt="Скачать в AppStore"/>
          </a>

          <a href="">
            <img src="/img/icons/mobile-app-gp-text.svg" alt="Скачать в Google Play"/>
          </a>
        </div>

      </aside>
    );
  }
}
