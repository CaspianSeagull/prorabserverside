import React  from 'react';
import {Link} from 'react-router';
import css    from './style.styl';

export default class Breadcrumbs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      links: []
    };
  }

  componentWillMount() {
    let links = this.props.links;

    if (links !== undefined && links.length) {
      this.setState({links});
    }
  }

  render() {
    return (
      <div className={css.Breadcrumbs}>
        <nav>
          <Link to="/">Главная</Link>
          {
            this.state.links.map(link => {
              return <Link key={`link_${link.url}`} to={link.url}>{link.text}</Link>
            })
          }
          <span>{this.props.current}</span>
        </nav>
      </div>
    );
  }
}
