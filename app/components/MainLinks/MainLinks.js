import React from 'react';
import {Link} from 'react-router';

import css from './style.styl';

export default class MainLinks extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={css.MainLinks}>
        <div className="row">
          <div className="col-xs-12">
            <h4>Что Вас интересует?</h4>
          </div>

          {this.renderServiceLinks()}
          {this.renderMobileLinks()}
        </div>
      </div>
    );
  }

  renderServiceLinks() {
    return(
      <nav>
        <div className="col-xs-12 col-sm-6 col-md-4">
          <div className={css.linkGroup}>
            <h2><Link to="/catalog/transport/rent">Спецтехника</Link></h2>
            <Link to="/catalog/transport/rent">Аренда</Link>
            <Link to="/catalog/transport/sell">Продажа</Link>
            <Link to="/catalog/transport/service">Запчасти и сервис</Link>
          </div>

          <div className={css.linkGroup}>
            <h2><Link to="/catalog/equipment/rent">Строительное оборудование</Link></h2>
            <Link to="/catalog/equipment/rent">Аренда</Link>
            <Link to="/catalog/equipment/sell">Продажа</Link>
          </div>

          <div className={css.linkGroup}>
            <h2><Link to="/catalog/shops">Магазины и стройматериалы</Link></h2>
          </div>
        </div>


        <div className="col-xs-12 col-sm-6 col-md-4">
          <div title="Раздел в разработке" className={css.linkGroup}>
            <h2><Link to="/catalog/service/order">Услуги</Link></h2>
            <Link to="/catalog/service/order">Заказы</Link>
            <Link to="/catalog/service/order">Предложения</Link>
          </div>

          <div className={css.linkGroup}>
            <h2><Link to="/catalog/cargoes/routes">Грузы и грузоперевозки</Link></h2>
            <Link to="/catalog/cargoes/routes">Рассчет маршрутов</Link>
            <Link to="/catalog/cargoes/search">Поиск груза</Link>
            <Link to="/catalog/cargoes/logistic">Перевозка</Link>
          </div>

          <div title="Раздел в разработке" className={css.linkGroup}>
            <h2><Link to="/catalog/vacancy/all">Работа</Link></h2>
            <Link to="/catalog/vacancy/all">Вакансии</Link>
            <Link to="/catalog/resume/all">Резюме</Link>
          </div>
        </div>
      </nav>
    );
  }

  renderMobileLinks() {
    return(
      <div className="col-xs-12 col-sm-12 col-md-4">
        <div className={css.MobileLinks}>
          <img src="/img/main-hand.png" alt=""/>
          <h2>Все строительные<br />услуги в твоем телефоне!</h2>

          <Link to=""><img src="/img/main-button-as.svg" alt=""/></Link>
          <Link to=""><img src="/img/main-button-gp.svg" alt=""/></Link>
        </div>
      </div>
    );
  }
}
