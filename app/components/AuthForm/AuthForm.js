import React from 'react';
import axios from 'axios';
import Notify from 'react-notification-system';
import ReactTelInput from 'react-telephone-input';

import API from '../../API';

import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import * as UserActions from '../../redux/actions/UserActions';

import css from './style.styl';





export class AuthForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      phone: '',
      passcode: '',
      verify: false,
      fetching: false
    };
  }

  notify(title, message, level) {
    this.refs.Notify.addNotification({title, message, level, position: 'tc'});
  }

  handlePhoneChange(phone) {

    // Чистим строку от пробелов и тире
    let cleanPhone =
      phone
      .split('-').join('')
      .split(' ').join('');

    console.log(cleanPhone);

    this.setState({phone: cleanPhone});
  }

  handlePCodeChange(event) {
    this.setState({passcode: event.target.value});
  }

  sendPhoneNumber() {
    axios
      .post(API.Accounts.getPasscode, {phone: this.state.phone})
      .then( response => {
        this.setState({verify: true});
        this.notify('', `Код подтверждения выслан на номер ${this.state.phone}`, 'success');
      })
      .catch( error => {
        let
          status = error.response.status,
          text   = error.response.statusText;

        if (status === 429) {
          this.setState({verify: true});
          this.notify('Внимание', 'На ваш номер уже было выслано смс с кодом подтверждения. Дождитесь кода и используйте его в форме авторизации.', 'warning');
        }

        else {
          this.notify('Ошибка', `${status}: ${text}`, 'error');
        }
      });
  }

  verifyUser() {
    this.props.UserActions.verifyUser( this.state.phone, parseInt(this.state.passcode));
  }

  handleSubmit(e) {
    e.preventDefault();

    this.state.verify
      ? this.verifyUser()
      : this.sendPhoneNumber();
  }

  render() {
    return (
      <div className={`${css.AuthForm} ${this.state.fetching ? css.fetching : ''}`}>
        <ReactTelInput
          defaultCountry="ru"
          value={this.state.phone}
          onChange={this.handlePhoneChange.bind(this)}
        />

        {this.state.verify ? <input onChange={this.handlePCodeChange.bind(this)} value={this.state.passcode} type="text" placeholder="Код подтверждения" /> : ''}

        <button onClick={this.handleSubmit.bind(this)}>
          {this.state.verify ? 'Отправить' : 'Получить код'}
        </button>

        <Notify ref="Notify"/>
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthForm);

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    UserActions: bindActionCreators(UserActions, dispatch)
  };
}