import React from 'react';

import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import Filter      from '../Filter/Filter';
import MobilePromo from '../MobilePromo/MobilePromo';

import * as AppActions from '../../redux/actions/AppActions';

import css from './style.styl';



class SideBar extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {
    let mapActive = this.props.app.mapActive;

    return (
      <div ref="container" className={`${css.SideBar}`}>
        <button
          className={`${css.mapToggle} ${mapActive ? css.mapActive : ''}`}
          onClick={this.switchMapState.bind(this)}    >
          {mapActive ? 'Скрыть карту' : 'Показать на карте'}
        </button>

        <Filter category={this.props.category}/>

        <MobilePromo />
      </div>
    );
  }



  switchMapState(e) {
    e.preventDefault();

    let mapActive = this.props.app.mapActive;

    if (mapActive)
      this.props.AppActions.disableMap();
    else
      this.props.AppActions.enableMap();
  }
}








// Connect component to store
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SideBar);

function mapStateToProps(state) {
  return {
    app:  state.app,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    AppActions:  bindActionCreators(AppActions, dispatch)
  };
}