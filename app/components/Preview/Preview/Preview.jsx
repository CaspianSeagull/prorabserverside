import React from 'react';

import UserAdActions from '../../UserAdActions/UserAdActions';

import {Link} from 'react-router';
import css    from './style.styl';

export default class Preview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inBookmarks: false
    };
  }

  /** Получаем заголовок объявления
   *
   * @param {object} adItem  - объект объявления
   * @returns {string} title - заголовок объявления
   */
  getTitle(adItem) {
    let title;

    switch(this.props.adType) {
      case 'transport':
      case 'equipment':
        let
          category = adItem.category ? adItem.category.name + ' ' : '',
          brand    = adItem.brand    ? adItem.brand.name    + ' ' : '',
          model    = adItem.model    ? adItem.model    + ' ' : '';

        title = category + brand + model;
        break;

      default:
        title = adItem.title || adItem.name;
        break;
    }

    return title;
  }

  /**
   * Переключение состояния закладки
   * @param {number} adid - id закладки
   *
   * @return {null} - nothing
   */
  toggleBookmark(adid) {
    if (this.props.inBookmarks)
      this.props.removeBookmark(this.props.adType, adid);

    else
      this.props.addBookmark(this.props.adType, adid);

    this.setState({
      inBookmarks: !this.state.inBookmarks
    });
  }

  componentDidMount() {
    // Запись типа объявления в state компонента
    this.setState({
      type: this.props.adType
    });
  }

  // Отрисовка компонента
  render() {
    let adItem = this.props.item;

    let photo = adItem.images && adItem.images.length ? adItem.images[0].thumbnail : null;
    let isVip = adItem.priority === 'vip';
    let title = this.getTitle(adItem);

    return (
      <div ref="adItem" className={`${css.Preview} ${isVip ? css.vip : ''}`}>
        <div className='row'>

          <div className='col-xs-12 col-md-3'>
            {renderPhoto(photo)}
          </div>

          <div className='col-xs-12 col-md-9'>
            {this.renderTitle(title, isVip)}

            <p>
              {adItem.description ? adItem.description : null}
            </p>

            <div className={css.info}>
              {
                adItem.hasOwnProperty('parameters')
                ? renderParameters(adItem.parameters)
                : <ul className={css.noparameters}></ul>
              }

              { renderPrice(adItem) }
            </div>

            {renderExtra(this.props.adType, adItem)}

            {renderBottom(adItem.address, adItem.id, this.props.adType)}
          </div>

        </div>
      </div>

    );
  }

  /** Отрисовка заголовка
   * @param {string} title
   * @param {boolean} vip
   *
   * @return {string} element - заголовок объявления
   */
  renderTitle(title, vip) {
    let bmButtonClass = this.props.inBookmarks ? css.bookmarkActive : css.bookmark;

    return (
      <div className={css.title}>
        <Link to={`/catalog/view/${this.props.adType}/${this.props.item.id}`}><h4>{title}</h4></Link>

        <div className={css.actions}>
          {vip ? <img src='/img/icons/vip.svg' alt=''/> : null}

          <UserAdActions
            ownerId={this.props.item.owner}
            type={this.props.adType}
            id={this.props.item.id}
            domRef={this.refs.adItem}
            fetchAds={this.props.fetchAds}
            isActive={this.props.item.is_active}
          />

          <button
            onClick={this.toggleBookmark.bind(this, this.props.item.id)}
            className={bmButtonClass}
          />
        </div>
      </div>
    );
  }
}

/** Отрисовка дополнительной информации
 * 
 * @param {string} adType - тип объявления
 * @param {object} adItem - объект объявления
 */
const renderExtra = (adType, adItem) => {
  let markup = null;

  switch(adType) {
    case 'transport':
    case 'equipment':
      markup = adItem.extra_equipment ?
        <div className={css.extraParams}>
          <h4>Дополнительное оборудование:</h4>
          <span>{adItem.extra_equipment}</span>
        </div> : null;
      break;

    case 'cargoes':
      markup =
        <div className={css.extraParams}>
          <span>Вес: <b>{adItem.weight}</b></span>
          <span>Цена: <b>{adItem.price}</b></span>
          <br />
          <span>Адрес доставки: <b>{adItem.to_address}</b></span>
        </div>;
      break;

    case 'shops':
      markup =
        <div className={css.extraParams}>
          <span>Телефон: <b>{adItem.phones[0]}</b></span>
        </div>;
      break;

    case 'service':
      markup =
        <div className={css.extraParams}>
          <span>Цена: <b>{adItem.price}</b></span>
        </div>;
      break;

    case 'vacancy':
    case 'resume':
      markup =
        <div className={css.extraParams}>
          <span>Зарплата: <b>{adItem.wages}</b></span>
        </div>;
      break;

    default: break;
  }

  return markup;};

/** Отрисовка цены в зависимости от типа предложения
 * @param {object} item - объект объявления
 */
const renderPrice = 
  item => {
    let result = null;

    if (item.offer_type === 'sell') {
      result = renderSellPrice( item.price );
    } else if (item.offer_type === 'rent') {
      result = renderRentPrice(item.price_per_hour, item.price_per_work_shift);
    }

    return result;
  };

/** Отрисовка кастомных параметров
 * @param {array} parameters - список объектов параметров
 */
const renderParameters =
  parameters => {
    let hiddenList = parameters.length > 4;



    return(
      <ul className={hiddenList ? css.parametersHidden : css.parameters}>
        {
          parameters ? parameters.map( param => {
              let value = param.value;

              if (param.type === 'boolean') {
                value = param.value ? 'да' : 'нет';
              }

              if (param.measure) {
                value += ` ${param.measure}`;
              }

              return(
                <li key={`transport_param_${param.slug}`}>
                  <span>{param.name}</span>
                  <b>{value}</b>
                </li>
              );
            }) : null
        }
      </ul>
    );
  };


/** Отрисовка адреса и ссылки
 * @param {string} address - адрес
 * @param {number} itemId - id объявления 
 */
const renderBottom =
  (address, itemId, type) =>
    <div className={css.extra}>
      <address>{address}</address>
      <Link to={`/catalog/view/${type}/${itemId}`}>Подробнее</Link>
    </div>;

/** Отрисовка фотографии
 *
 * @param image - ссылка на фото объявления
 * @returns {XML}
 */
const renderPhoto =
  image =>
    <div className={css.image}>
      <img src={image || '/img/no-photo.png'} alt=''/>
    </div>;


/** Отрисовка цены за аренду
 *
 * @param {number} hour - цена за час
 * @param {number} period - цена за смену
 */
const renderRentPrice =
  (hour, period) =>
    <div className={css.prices}>
      <b>{hour} ₽</b><span> / час</span>
      <br/>
      <b>{period} ₽</b><span> / смена</span>
    </div>;


/** Отрисовка цены за продажу
 *
 * @param {number} price
 */
const renderSellPrice =
  price =>
    <div className={css.prices}>
      <b>{price} ₽</b>
    </div>;