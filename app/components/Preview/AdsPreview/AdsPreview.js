import React from 'react';
import css    from './styles.styl';


export default class AdsPreview extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {
    return (
      <div className={css.Preview}>
        <div className="row">

          <div className="col-md-3">
            <div className={css.image}>
              <img src="/img/no-photo.png" alt=""/>
            </div>
          </div>

          <div className="col-md-9">

            { this.renderTitle() }

            <div className={css.info}>
              { this.renderParameters() }
              { this.renderPrices() }
            </div>

            { this.renderExtra() }

          </div>
        </div>
      </div>
    );
  }


  renderTitle() {
    return(
      <div className={css.title}>
        <h4>123</h4>

        <div className={css.actions}>
          <button className={css.bookmark} />
        </div>
      </div>
    );
  }
  renderParameters() {
    return(
      <ul className={css.parameters}>
      </ul>
    );
  }
  renderPrices() {
    return(
      <div className={css.prices}>
        <b>0</b><span />
        <br/>
        <b>0</b><span />
      </div>
    );
  }
  renderExtra() {
    return(
      <div className={css.extra}>
        <address />
        <a>Подробнее</a>
      </div>
    );
  }

}