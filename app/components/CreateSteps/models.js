export const models = {
  common: {
    images: [],
    location: {
      lat: 0,
      lon: 0
    },
    phones: ['', ''],
    address: '',
    state: 'state_1',
    is_active: true,
    priority: 'common'
  },

  transport: {
    model: '',
    category: 0,
    brand: 0,
    parameters: {},
    offer_type: 'rent',
    extra_equipment: '',
    price: 0,
    price_per_hour: 0,
    price_per_work_shift: 0,
    minimum_rental_period: 0
  },

  equipment: {
    model: '',
    category: 0,
    brand: 0,
    parameters: {},
    offer_type: 'rent',
    extra_equipment: '',
    price: 0,
    price_per_hour: 0,
    price_per_work_shift: 0,
    minimum_rental_period: 0
  },

  cargo: {
    name: '',
    description: '',
    price: 0,
    weight: 0,
    to_address: '',
    to_city: ''
  },

  shop: {
    name: '',
    description: '',
    category: 0,
    pricelist: {}
  },

  service: {
    name: '',
    description: '',
    parameters: {},
    price: 0,
    category: 0
  },

  vacancy: {
    title: '',
    description: '',
    wages: 0,
    category: 0
  },

  resume: {
    title: '',
    description: '',
    wages: 0,
    category: 0,
    resume: null
  }
};