import React       from 'react';
import axios       from 'axios';
import Notify      from 'react-notification-system';
import GoogleMap   from 'google-map-react';
import API         from '../../API';
import {Placemark} from '../Placemark/Placemark';
import AuthForm    from '../AuthForm/AuthForm';

import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';
import * as UserActions       from '../../redux/actions/UserActions';

import css from './style.styl';


// Компонент Третьего шага создания объявления
class Step3 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 3,

      isValid: false,

      photos: [],
      photosIDs: [],
      uploadingPhoto: false,

      authorized: false,
      waitingConfirm: false,

      verifyCode: '',

      currentAddress: '',
      currentLocation: {
        lat: 0,
        lng: 0
      },

      workState: {
        phones: ['', '']
      }
    };
  }

  //region Helpers

  // Установка шага создания объявления
  setStep(e) {
    e.preventDefault();

    this.saveWorkState();

    this.props.setStep(parseInt(e.target.dataset.step));
  }

  // Показываем уведомление
  notify(title, message, level) {
    this.refs.Notify.addNotification({title, message, level});
  }

  //endregion

  //region Data operations
  // Загрузка сохраненных фотографий
  restorePhotos() {
    let savedPhotos = localStorage.getItem('newAdPhotos');

    if(savedPhotos && savedPhotos !== 'undefined') {
      this.setState({
        photos: JSON.parse(savedPhotos)
      });
    }
  }

  // Сохранение данных для следующего шага
  saveWorkState() {
    const newAdBackup = {
      currentType: this.state.currentType,
      categoryId:  this.state.categoryId,
      workState:   this.state.workState
    };

    localStorage.setItem('newAdBackup', JSON.stringify(newAdBackup));
    localStorage.setItem('newAdPhotos', JSON.stringify(this.state.photos));
  }

  // Загрузка данных с предыдущего шага
  loadLocalWorkState() {
    try {
      const backup = JSON.parse(localStorage.getItem('newAdBackup'));

      this.setState({
        currentType: backup.currentType,
        categoryId:  backup.categoryId,
        workState:   backup.workState
      });

      this.restorePhotos();

    } catch(error) {
      this.notify('Ошибка', 'Ошибка загрузки рабочей модели. Проверьте консоль', 'error');
      console.warn(error);
    }
  }

  // Проверка валидации
  checkValidation() {
    let lat = this.state.workState.location.lat;
    let lon = this.state.workState.location.lon;

    let validLocation =
      !isNaN(lat) && !isNaN(lon) &&
      lat !== 0   && lon !== 0;

    let validPhones =
      Array.isArray(this.state.workState.phones) &&
      this.state.workState.phones[0];

    let isValid = validPhones && validLocation;

    this.saveWorkState();

    this.setState({isValid});
  }

  // Установка координат пользователя на карте по умолчания
  initMapCoordinates() {
    let localAddress  = localStorage.getItem('userLocation');

    if (localAddress !== null && localAddress !== 'undefined') {
      localAddress = JSON.parse(localAddress);

      this.setState({
        currentLocation: localAddress
      });

      this.handleMapClick(localAddress);
    }
  }

  // Установка номера телефона пользователя
  initUserPhone() {
    let localUserInfo = localStorage.getItem('userInfo');

    if (localUserInfo !== null && localUserInfo !== 'undefined') {
      localUserInfo = JSON.parse(localUserInfo);

      this.setState({
        phones: [localUserInfo.phone || '','']
      });
    }
  }

  // Добавляем загруженную фотографию на страницу
  addPhotoToPostForm(data) {
    let
      imgId        = data.id,
      newPhotosIDs = this.state.photosIDs.concat(imgId),
      newPhotos    = this.state.photos.concat(data);

    this.setState({
      photosIDs: newPhotosIDs,
      photos:    newPhotos
    }, this.setWorkStatePhotos.bind(this));
  }

  // Удаляем фотографию
  deleteTempPhoto(e) {
    let id = parseInt(e.target.dataset.id);

    let
      newPhotos    = this.state.photos.filter( photo => photo.id !== id ),
      newPhotosIDs = newPhotos.map( item => {return {id: item.id};});

    this.setState({
      photos:    newPhotos,
      photosIDs: newPhotosIDs
    }, this.setWorkStatePhotos.bind(this));
  }

  // Загружаем фотографию на сервер
  uploadPhoto(e, fileFormData, isDrop) {
    let token = localStorage.getItem('clientToken');

    if (token !== null) {
      let formData = isDrop
        ? fileFormData
        : new FormData(document.forms.addPhoto);

      this.setState({uploadingPhoto: true});

      axios
        .post(API.Ads.images, formData, {headers: {'Authorization': `Token ${token}`}})
        .then( response => {
          let data = response.data;
          this.addPhotoToPostForm(data);
          this.setState({uploadingPhoto: false});
        })
        .catch(error => console.warn(error));
    }

    else console.info('[APP] User not authorised');
  }

  // Запись данных о фотографиях в модель объявления
  setWorkStatePhotos() {
    let images = this.state.photosIDs;
    let workState = Object.assign({}, this.state.workState, {images});
    this.setState({workState}, () => {this.saveWorkState();});
  }

  // Запись указанных координат в модель объявления
  setWorkStateLocation() {
    let workState = Object.assign({}, this.state.workState, {
      address: this.state.currentAddress,
      location: {
        lat: this.state.currentLocation.lat.toFixed(6),
        lon: this.state.currentLocation.lng.toFixed(6)
      }
    });

    this.setState({workState}, () => {this.checkValidation();});
  }

  //endregion

  //region Handlers

  // Обработка клика по карте
  handleMapClick(coords) {
    let
      lat = coords.lat,
      lng = coords.lng;

    axios
      .get(API.Google.coordsToAddress(lat, lng))
      .then( response => {

        this.setState({
          currentAddress:  response.data.results[0].formatted_address,
          currentLocation: {lat: lat, lng: lng}
        }, this.setWorkStateLocation.bind(this) );

      })
      .catch(error => {
        this.notify('Ошибка', 'Указаны неверные координаты', 'warning');
      });
  }

  // Обработка изменения номера телефона
  handlePhoneChange(e) {
    let phone = e.target.value;
    let index = parseInt(e.target.dataset.index);

    let phones = this.state.workState.phones;

    phones[index] = phone;

    let workState = Object.assign({}, this.state.workState, {phones});

    this.setState({workState}, this.checkValidation());
  }

  // Обработка изменения кода подтвержения
  handleVerifyCodeChange(e) {
    let code = parseInt(e.target.value);

    this.setState({
      verifyCode: code
    });
  }

  // Обрабатываем перетаскивание фотографий в окно
  handlePhotoDrag(e) {
    e.preventDefault();
    e.target.parentElement.classList.add(css.dragging);
  }

  // Обрабатываем сброс фотографий в область загрузки
  handlePhotoDrop(e) {
    e.preventDefault();
    e.target.classList.remove(css.dragging);

    let files = e.dataTransfer.files;
    let count = files.length;

    for (let i = 0; i <= count; i++) {
      let fileFormData = new FormData();
      fileFormData.append('image', files[i]);

      this.uploadPhoto(e, fileFormData, true);
    }
  }
  //endregion

  //region Fetching

  checkAuthorization() {
    this.setState({authorized: this.props.user.authorized});
  }
  //endregion

  //region Lifecycle
  componentDidMount() {
    this.loadLocalWorkState();

    this.checkAuthorization();

    this.initMapCoordinates();
    this.initUserPhone();
  };
  //endregion

  //region Render
  render() {
    return (
      <section className={css.Step}>

        {this.props.user.authorized ? this.renderPhonesSelect() : this.renderNumberValidation()}
        {this.renderAddPhotosForm()}

        {this.props.user.authorized ? this.renderMap() : null}

        <nav>
          <button data-step="2" onClick={this.setStep.bind(this)}>Назад</button>
          <button disabled={!this.state.isValid} data-step="4" onClick={this.setStep.bind(this)}>
            {this.state.isValid && this.props.user.authorized ? 'Завершение' : 'Укажите данные'}
          </button>
        </nav>

        <Notify ref="Notify"/>
      </section>
    );
  }

  // Запись номера телефона
  renderPhonesSelect() {
    let userPhone = this.props.user.authorized ? this.props.user.info.phone : this.state.workState.phones[0];

    return(
      <div className={css.paramGroupList}>

          <div className={`${css.formGroup} ${css.formGroupHalf}`}>
            <label>Ваш номер</label>

            <input
              type="phone"
              data-index="0"
              defaultValue={userPhone}
              onChange={this.handlePhoneChange.bind(this)}
            />
          </div>

          {/*<div className={`${css.formGroup} ${css.formGroupHalf}`}>*/}
            {/*<label>Дополнительный номер</label>*/}

            {/*<input*/}
              {/*type="phone"*/}
              {/*data-index="1"*/}
              {/*value={this.state.workState.phones[1]}*/}
              {/*onChange={this.handlePhoneChange.bind(this)}*/}
            {/*/>*/}
          {/*</div>*/}

      </div>
    );
  }


  renderNumberValidation() {
    return (
      <div className={css.Auth}>
        <p>Для публикации объявления подтвердите свой номер телефона.<br />На ваш номер будет выслано бесплатное смс с паролем.</p>
        <AuthForm />
      </div>
    );
  }

  // Отрисовка фотографии
  renderPhoto(photo) {
    return(
      <div key={photo.id} className={css.photoItem}>
        <img src={photo.thumbnail} />
        <span data-id={photo.id} onClick={this.deleteTempPhoto.bind(this)} className={css.delPhoto} />
      </div>
    );
  }

  // Форма добавления фотографий
  renderAddPhotosForm() {
    if (this.props.user.fetching === 'ready') {
      return (
        <div className={css.paramGroupList}>

          <div className={css.photos}>
            <div className={css.photoList}>
              { this.state.photos.map( photo => this.renderPhoto( photo ) ) }
            </div>

            {
              this.state.photos.length < 5 ?
                <form name="addPhoto">
                  <div className={`${css.addPhoto} ${this.state.uploadingPhoto ? css.uploading : ''}`}>

                    <h3>Нажмите, чтобы загрузить фотографии</h3>
                    <span>или</span>
                    <h3>перетащите их сюда</h3>

                    <input
                      type="file" name="image"
                      onDragOver={this.handlePhotoDrag.bind( this )}
                      onDrop={this.handlePhotoDrop.bind( this )}
                      onChange={this.uploadPhoto.bind( this )}
                    />

                  </div>
                </form>
                : 'Максимум 5 фотографий'
            }
          </div>
        </div>
      );
    } else return null;
  }

  // Отрисовка карты и метки
  renderMap() {
    return(
      <div className={css.paramGroupList}>

        <div className={css.Map}>
          <div className={css.mapContainer}>
            <input
              title="Выберите точку на карте, чтобы указать адрес"
              value={this.state.currentAddress}
              type="text" placeholder="Выберите точку на карте" readOnly="true"
            />

            <GoogleMap
              onClick={this.handleMapClick.bind(this)}
              center={this.state.currentLocation}
              zoom={12}
              bootstrapURLKeys={{
                key: 'AIzaSyA1l_CX9YVk3O_qywJUaVT0RIiRK49AFQw',
                language: 'ru'
              }}>

              <Placemark
                category="transport"
                lat={this.state.currentLocation.lat}
                lng={this.state.currentLocation.lng}
              />

            </GoogleMap>
          </div>
        </div>

      </div>
    );
  }
  //endregion
}

export default connect(mapStateToProps, mapDispatchToProps)(Step3);
function mapStateToProps(state) {
  return {
    user: state.user
  };
}
function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(UserActions, dispatch)
  };
}