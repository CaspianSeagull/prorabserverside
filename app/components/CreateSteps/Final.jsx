import React   from 'react';
import axios   from 'axios';
import Notify  from 'react-notification-system';
import API     from '../../API';
import css     from './style.styl';


export default class Final extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 4,

      currentType: 'none',
      uploadingAds: true,

      workState: undefined
    };
  }

  //region Helpers

  // Установка шага создания объявления
  setStep(e) {
    e.preventDefault();

    this.props.setStep(parseInt(e.target.dataset.step));
  }

  // Показываем уведомление
  notify(title, message, level) {
    this.refs.Notify.addNotification({title, message, level, position: 'tc'});
  }

  //endregion

  //region Data operations
  loadLocalWorkState() {
    try {
      const backup = JSON.parse(localStorage.getItem('newAdBackup'));

      this.setState({
        currentType: backup.currentType,
        categoryId:  backup.categoryId,
        workState:   backup.workState
      }, () => {
        this.props.edit ? this.patchAdToServer() : this.uploadAdToServer();
      });

    } catch(error) {
      this.notify('Ошибка', 'Ошибка загрузки рабочей модели. Проверьте консоль', 'error');
      console.warn(error);
    }
  }
  //endregion

  uploadAdToServer() {
    let token = localStorage.getItem('clientToken');

    let ad = Object.assign({}, this.state.workState);

    if (ad.phones[1] === '') {
      let phone = ad.phones[0];

      ad.phones = [phone];
    }

    if (token !== null) {
      axios
        .post(API.Ads.uploadItem(this.state.currentType), ad, {
          headers: {
            'Authorization': `Token ${token}`,
            'Content-Type': 'application/json'
          }
        })
        .then(response => {
          this.notify('Загружено!', 'Ваше объявление успешно загружено', 'success');

          localStorage.removeItem('newAdBackup');
          localStorage.removeItem('newAdPhotos');

          this.setState({
            success: true,
            uploadingAds: false,
            newAdId: response.data.id
          });
        })
        .catch(error => {
          this.notify('Ошибка', 'Не удалось загрузить объявление', 'error');

          this.setState({
            success: false,
            uploadingAds: false,
          });
        });
    }
  }

  patchAdToServer() {
    // Берем токен
    let token = localStorage.getItem('clientToken');

    // Клонируем объект объявления
    let ad = Object.assign({}, this.state.workState);

    // Чистим номера телефонов, если второй не указан
    if (ad.phones[1] === '') {
      let phone = ad.phones[0];

      ad.phones = [phone];
    }

    // Правим формат массива с id фотографий
    // Получаем массив с id
    let fixedImagesArray = ad.images.map( image => typeof(image) === 'number' ? image : image.id );
    // И расширяем объект объявления этими параметрами
    ad = Object.assign({}, this.state.workState, {images: fixedImagesArray});

    // Правим категорию и бренд
    ad.brand ? ad.brand = ad.brand.id : null;
    ad.category ? delete ad.category : null;

    // Если токен не пустой, отправляем PATCH-запрос
    if (token !== null) {
      axios
        .patch(API.Ads.item(this.state.currentType, this.props.editId), ad, {
          headers: {
            'Authorization': `Token ${token}`,
            'Content-Type': 'application/json'
          }
        })
        .then(response => {
          this.notify('Обновлено!', 'Ваше объявление успешно обновлено', 'success');

          localStorage.removeItem('newAdBackup');
          localStorage.removeItem('newAdPhotos');

          this.setState({
            success: true,
            uploadingAds: false,
            newAdId: response.data.id
          });
        })
        .catch(error => {
          this.notify('Ошибка', 'Не удалось обновить объявление', 'error');

          this.setState({
            success: false,
            uploadingAds: false,
          });
        });
    }
  }

  //region Lifecycle
  componentDidMount() {
    this.loadLocalWorkState();
  };
  //endregion

  //region Render
  render() {
    return (
      <section className={css.Step}>
        {
          this.state.uploadingAds ? (
            <h2>Публикуем объявление...</h2>
          ) : this.state.success ? (
              <div>
                <h2>Объявление {this.props.edit ? ' изменено' : ' опубликовано'}
                </h2>

                <a href={`/catalog/view/${this.state.currentType}/${this.state.newAdId}`}>Посмотреть</a>
              </div>

            ) : (
              <div>
                <h2>Произошла ошибка</h2>
                <button data-step="3" onClick={this.setStep.bind(this)}>Вернуться</button>
                <button onClick={this.uploadAdToServer.bind(this)}>Попробовать еще раз</button>
              </div>
            )
        }
        <Notify ref='Notify'/>
      </section>
    );
  }
  //endregion
}