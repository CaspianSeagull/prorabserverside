import React   from 'react';
import axios   from 'axios';
import Notify  from 'react-notification-system';
import API     from '../../API';
import Loading from '../../components/Loading/Loading';
import css     from './style.styl';

// Компонент второго шага создания объявления
export default class Step2 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 2,

      isValid: false,

      currentType: 'none',
      categoryId: undefined,

      brands: [],
      models: [],

      fetchingModels: true,
      fetchingBrands: true,
      fetchingParams: true,

      customParameters: [],
      customParametersState: {},

      currentBrandId: undefined,
      currentModelId: undefined,

      workState: {}
    };
  }

  //region Helpers

  // Установка шага создания объявления
  setStep(e) {
    e.preventDefault();

    this.props.setStep(parseInt(e.target.dataset.step));
  }

  // Показываем уведомление
  notify(title, message, level) {
    this.refs.Notify.addNotification({title, message, level});
  }

  //endregion

  //region Data operations

  // Сохранение данных для следующего шага
  saveWorkState() {
    const newAdBackup = {
      currentType: this.state.currentType,
      categoryId:  this.state.categoryId,
      workState:   this.state.workState
    };

    localStorage.setItem('newAdBackup', JSON.stringify(newAdBackup));
  }

  // Загрузка данных с предыдущего шага
  loadLocalWorkState() {
    try {
      const backup = JSON.parse(localStorage.getItem('newAdBackup'));

      this.setState({
        currentType: backup.currentType,
        categoryId:  backup.categoryId,
        workState:   backup.workState
      }, () => {
        this.checkValidation();
        this.fetchBrandsList();
        this.fetchCustomParameters();
      });

    } catch(error) {
      this.notify('Ошибка', 'Ошибка загрузки рабочей модели. Проверьте консоль', 'error');
      console.warn(error);
    }
  }

  // Запись id модели транспорта или оборудования
  setModel(id) {
    let workState = Object.assign({}, this.state.workState, {
      model: id
    });

    this.setState({workState}, () => {
      this.notify('Готово', 'Модель установлена', 'info');
      this.checkValidation();
    });
  }

  setBrand(id) {
    let workState = Object.assign({}, this.state.workState, {
      brand: id
    });

    this.setState({workState}, () => {
      this.checkValidation();
    });
  }

  // Проверка правильности указанных параметров
  checkValidation() {
    let isValid = false;

    let validModel = false;
    let validPrice = false;
    let validTitle = false;

    switch(this.state.currentType) {
      case 'transport':
      case 'equipment':
        validModel = this.state.workState.model !== '';

        if (this.state.workState.offer_type === 'rent') {
          validPrice =
            !!this.state.workState.price_per_hour &&
            !!this.state.workState.price_per_work_shift &&
            !!this.state.workState.minimum_rental_period;
        }
        else if (this.state.workState.offer_type === 'sell') {
          validPrice =
            !!this.state.workState.price;
        }

        isValid = validModel && validPrice;
        break;

      case 'service':
        validPrice =
          !!this.state.workState.price;

        validTitle =
          !!this.state.workState.name &&
          !!this.state.workState.description;

        isValid = validPrice && validTitle;
        break;

      case 'cargo':
        let walidWeight =
          !!this.state.workState.weight;

        validPrice =
          !!this.state.workState.price;

        validTitle =
          !!this.state.workState.name &&
          !!this.state.workState.description;

        isValid = walidWeight && validPrice && validTitle;
        break;

      case 'shop':
        validTitle =
          !!this.state.workState.name &&
          !!this.state.workState.description;

        isValid = validTitle;
        break;

      case 'vacancy':
      case 'resume':
        validTitle =
          !!this.state.workState.title &&
          !!this.state.workState.description;

        validPrice =
          !!this.state.workState.wages;

        isValid = validPrice && validTitle;
        break;

      default: break;
    }

    this.saveWorkState();

    this.setState({isValid});
  }

  //endregion

  //region Handlers

  // Изменение параметра модели
  handleParamChange(e) {
    let paramName = e.target.dataset.param, newValue = {};

    console.log(e.target.type);

    if (e.target.type === 'number') {
      newValue[paramName] = parseInt(e.target.value);
    } else {
      newValue[paramName] = e.target.value;
    }

    const workState = Object.assign({}, this.state.workState, newValue);

    this.setState({workState}, this.checkValidation());
  }

  // Изменение кастомного параметра
  handleCustomParamChange(e, isBoolean=false) {
    if (this.state.workState.hasOwnProperty('parameters')) {
      let parametersList = this.state.workState.parameters;
      let slug  = e.target.dataset.slug;

      if (e.target.type === 'number') {
        parametersList[slug] = parseInt(e.target.value);
      } else {
        parametersList[slug] = e.target.value;
      }

      if (e.target.dataset.boolean) {
        parametersList[slug] = !!e.target.value;
      }

      let newWorkState = Object.assign({}, this.state.workState, {parameters: parametersList});

      this.setState({
        workState: newWorkState
      }, this.checkValidation());
    } else {
      return false;
    }
  }

  // Изменение бренда
  handleBrandChange(e) {
    console.log(e);
    let brandID = parseInt(e.target.value);

    this.setBrand(brandID);
  }

  // Изменение модели
  handleModelChange(e) {
    let modelId = parseInt(e.target.value);

    this.setModel(modelId);
  }

  //endregion

  //region Fetching
  fetchBrandsList() {
    this.setState({fetchingBrands: true});

    let
      type =       this.state.currentType,
      categoryId = this.state.categoryId;

    let requestUrl = null;

    switch(type) {
      case 'transport': requestUrl = API.Transport.types.brands(categoryId); break;
      case 'equipment': requestUrl = API.Equipment.types.brands(categoryId); break;
      default: requestUrl = null;
    }

    if (requestUrl) {
      axios.get(requestUrl)
        .then(response => {
          this.setState({
            fetchingBrands: false,
            brands: response.data
          });
        })
        .catch(error => {
          this.notify('Ошибка', 'Ошибка загрузки брендов в категории', 'warning');
          console.warn(error);
        });
    }
  }
  fetchCustomParameters() {
    this.setState({fetchingParams: true}, () => {

      let
        type       = this.state.currentType,
        categoryId = this.state.categoryId;

      let requestUrl = null;

      switch(type) {
        case 'transport': requestUrl = API.Transport.params(categoryId); break;
        case 'equipment': requestUrl = API.Equipment.params(categoryId); break;
        default: requestUrl = null;
      }

      if (requestUrl) {
        axios.get(requestUrl)
          .then(response => {

            this.setState({
              fetchingParams: false,
              customParameters: response.data
            });

          })
          .catch(error => {
            this.notify('Ошибка', 'Не удалось загрузить параметры', 'warning');

            console.warn(error);
          });
      }

    });
  }
  //endregion

  //region Lifecycle
  componentDidMount() {
    this.loadLocalWorkState();
  };

  componentWillReceiveProps() {
    this.loadLocalWorkState();
  };
  //endregion

  //region Render
  /** Отрисовка компонента на странице */
  render() {
    return (
      <section className={css.Step}>

        {this.renderModelSelect()}
        {this.renderPriceSelect()}
        {this.renderInformation()}

        {this.renderCustomParameters()}

        {this.props.edit ? null : this.renderNavigation()}

        <Notify ref='Notify'/>
      </section>
    );
  }

  // Блок выбора модели транспорта или оборудования
  renderModelSelect() {
    let type = this.state.currentType;

    if (type === 'transport' || type === 'equipment') {
      return(
        <div className={css.paramGroupList}>
          {
            this.state.brands.length
              ? this.renderBrandsList()
              : <div className={css.formGroup}>
                  <label>Выберите бренд</label>
                  <select disabled>
                    <option>Нет брендов</option>
                  </select>
                </div>

          }

          { this.renderParamField('model', 'Модель', 'text') }
        </div>
      );
    } else return null;
  }
  renderBrandsList() {
    if (this.state.fetchingBrands) {

      return (
        <div className={css.formGroup}>
          <label>Выберите бренд</label>
          <select disabled>
            <option>Загрузка...</option>
          </select>
        </div>
      );

    } else {

      return(
        <div className={css.formGroup}>
          <label>Выберите бренд</label>
          <select onChange={this.handleBrandChange.bind(this)}>
            {
              this.state.brands.map(
                brand => <option key={'brand_' + brand.id} value={brand.id}>{brand.name}</option>
              )
            }
          </select>
        </div>
      );

    }
  }

  // Блок выбора цены
  renderPriceSelect() {
    let isRent = this.state.workState.offer_type === 'rent';
    let type = this.state.currentType;

    if (type === 'transport' || type === 'equipment') {

      if (isRent) {
        return(
          <div className={css.paramGroupList}>
            { this.renderParamField('price_per_hour',        'цена за час',              'number', 'Р.') }
            { this.renderParamField('price_per_work_shift',  'цена за смену',            'number', 'Р.') }
            { this.renderParamField('minimum_rental_period', 'Минимальное время аренды', 'number', 'Ч.') }
          </div>
        );
      } else {
        return(
          <div className={css.paramGroupList}>
            { this.renderParamField('price', 'цена', 'number', 'Р.') }
          </div>
        );
      }

    } else if (type === 'vacancy' || type === 'resume') {

      return(
        <div className={css.paramGroupList}>
          { this.renderParamField('wages', 'зарплата', 'number', 'Р.') }
        </div>
      );

    } else if (type === 'cargo') {
      return(
      <div className={css.paramGroupList}>
        { this.renderParamField('weight', 'вес',  'number', 'Кг.') }
        { this.renderParamField('price',  'цена', 'number', 'Р.') }
      </div>
  );
    } else {
      return(
        <div className={css.paramGroupList}>
          { this.renderParamField('price', 'цена', 'number', 'Р.') }
        </div>
      );
    }
  }

  // Блок названия и описания
  renderInformation() {
    return(
      <div className={css.paramGroupList}>
        { this.renderParamField('to_address',  'Адрес доставки', 'text', '', true) }
        { this.renderParamField('name',        'Заголовок',      'text', '', true) }
        { this.renderParamField('title',       'Заголовок',      'text', '', true) }
        { this.renderParamField('description', 'Описание',       'text') }
      </div>
    );

  }

  // Блок кастомных параметров
  renderCustomParameters() {
    let type = this.state.currentType;

    if (type === 'transport' || type === 'equipment') {

      if (this.state.fetchingParams) {

        return(
          <div className={css.parameters}>
            <h5>Загрузка параметров</h5>
            <Loading />
          </div>
        );

      } else {

        return(
          <div className={css.paramGroupList}>
            {
              this.state.customParameters.map(
                param => this.renderCustomParamField(param)
              )
            }
          </div>
        );

      }

    }
  }

  // Форма изменения параметра модели
  renderParamField(propName, label, type, postfix, wide=false) {
    if(this.state.workState.hasOwnProperty(propName)) {

      if(propName === 'description') {
        return (
          <div className={css.formGroupDesc}>
            <label>{label}</label>

            <textarea
              type={type}
              data-param={propName}
              value={this.state.workState[propName] || ''}
              onChange={this.handleParamChange.bind( this )}
            >
            </textarea>

          </div>
        );

      } else {
        return (
          <div className={wide ? css.formGroupDesc : css.formGroup}>
            <label>{label}</label>

            <input
              type={type}
              data-param={propName}
              value={this.state.workState[propName]}
              onChange={this.handleParamChange.bind( this )}
            />

            { postfix ? (<span>{postfix}</span>) : null}
          </div>
        );
      }
    }
  }

  // Форма изменения кастомного параметра
  renderCustomParamField(param) {
    const typeFix = {
      integer: 'number',
      string:  'text',
      boolean: 'boolean'
    };

    let paramField = null;

    let type = typeFix[param.type];

    switch(type) {
      case 'text':
        paramField =
          <select data-slug={param.slug} onChange={this.handleCustomParamChange.bind(this)}>
            <option value=''>Не выбрано</option>
            {
              param.values ? param.values.map(
                paramValue => <option key={paramValue} value={paramValue}>{paramValue}</option>
              ) : ''
            }
          </select>;
        break;

      case 'number':
        paramField =
          <input
            data-slug={param.slug}
            type='number'
            onChange={this.handleCustomParamChange.bind(this)}
          />;
        break;

      case 'boolean':
        paramField =
          <select data-boolean={1} data-slug={param.slug} onChange={this.handleCustomParamChange.bind(this)}>
            <option value={false}>Нет</option>
            <option value={true} >Да </option>
          </select>;
        break;

      default: break;
    }


    return (
      <div key={`param_${param.slug}`} className={css.formGroup}>
        <label>{param.name}</label>
        {paramField}

        { param.measure ? (<span>{param.measure}</span>) : null}
      </div>
    );
  }

  // Навигация по шагам
  renderNavigation() {
    return(
      <nav>
        <button data-step='1' onClick={this.setStep.bind(this)}>Назад</button>
        <button data-step='3' onClick={this.setStep.bind(this)} disabled={!this.state.isValid}>
          {this.state.isValid ? 'Следующий шаг' : 'Укажите данные'}
        </button>
      </nav>
    );
  }
  //endregion
}