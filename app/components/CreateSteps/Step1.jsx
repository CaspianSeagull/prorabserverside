import React    from 'react';
import axios    from 'axios';
import Notify   from 'react-notification-system';
import API      from '../../API';
import {models} from './models';
import css      from './style.styl';

// Компонент первого шага создания объявления
export default class Step1 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // Шаг создания обяъвления
      step: 1,

      // Проверка валидности указанных параметров
      isValid: false,

      // Категории по типу обявления
      categories: [],

      /** Текущий тип объявления
         по умолчанию none, потому что при value={undefined}
         возвращается текст option'а, а не undefined */
      currentType: 'none',

      // ID выбранной категории
      categoryId: undefined,

      // Статус ожидания результатов запроса
      fetching: true,

      // Рабочее состояние модели объявления
      workState: {},

      // Шаблоны моделей объявлений
      models
    };
  }

  //region Helpers

  // Установка шага создания объявления
  setStep(e) {
    e.preventDefault();

    this.props.setStep(parseInt(e.target.dataset.step));
  }

  // Показываем уведомление
  notify(title, message, level) {
    this.refs.Notify.addNotification({title, message, level});
  }
  //endregion

  //region Data operations

  setType(type, restore) {
    // Формируем модель объявления для отправки на сервер из json-заготовок
    let
      commonModel   = this.state.models.common,
      selectedModel = this.state.models[type],
      workState     = Object.assign({}, commonModel, selectedModel);

    let newState;

    if (restore) {
      newState = {currentType: type};
    } else {
      newState = {categoryId: undefined, workState, currentType: type};
    }

    this.setState(newState, () => {
      this.checkValidation();
      this.fetchCategories(restore);
    });
  }

  // Проверка правильности указанных параметров
  checkValidation() {
    let isValid = false;

    let validType = this.state.currentType !== 'none' && !!this.state.currentType;

    let validOfferType = false;
    let offerTypes = ['sell', 'rent', 'offer', 'order'];

    switch(this.state.currentType) {
      case 'none':
        isValid = false;
        break;

      case 'transport':
      case 'equipment':
        validOfferType = offerTypes.indexOf(this.state.workState.offer_type) !== -1;

        isValid = validType && validOfferType && !!this.state.categoryId;
        break;

      case 'service':
        validOfferType = offerTypes.indexOf(this.state.workState.type) !== -1;

        isValid = validType && validOfferType && !!this.state.categoryId;
        break;

      case 'cargo':
        isValid = validType;
        break;

      default:
        isValid = validType && !!this.state.categoryId;
        break;
    }

    this.saveWorkState();

    this.setState({isValid});
  }

  saveWorkState() {
    const newAdBackup = {
      currentType: this.state.currentType,
      categoryId:  this.state.categoryId,
      workState:   this.state.workState
    };

    localStorage.setItem('newAdBackup', JSON.stringify(newAdBackup));
  }

  restoreWorkState() {
    let localData = localStorage.getItem('newAdBackup');

    if (localData === null) return false;

    const backup = JSON.parse(localData);

    this.setState({
      currentType: backup.currentType,
      categoryId:  backup.categoryId,
      workState:   backup.workState
    }, () => {
      this.setType(backup.currentType, true);
      this.props.edit ? '' : this.notify('Данные восстановлены', '', 'success');
    });
  }
  //endregion

  //region Fetching
  // Загрузка категорий с сервера
  fetchCategories(restore) {
    let type = this.state.currentType;

    this.setState({fetching: true});

    if (type === 'cargo' || type === 'none' || type === undefined) return false;

    let paramUrl = '';

    switch(type) {
      case 'transport':
        paramUrl = API.Transport.types.categories;
        break;

      case 'equipment':
        paramUrl = API.Equipment.types.categories;
        break;

      case 'shop':
        paramUrl = API.Shop.categories;
        break;

      case 'service':
        paramUrl = API.Service.categories;
        break;

      case 'vacancy':
      case 'resume':
        paramUrl = API.Job.categories;
        break;

      default: break;
    }

    axios
      .get(paramUrl)
      .then( response => {
        let newState;

        if (restore) {
          newState = {
            categories: response.data,
            fetching: false
          };
        } else {
          newState = {
            categories: response.data,
            categoryId: undefined,
            fetching: false
          };
        }

        this.setState(newState);

        console.info('[APP] Категории загружены');

      })
      .catch( error => {
        this.notify('Ошибка', 'Ошибка загрузки категорий', 'warning');

        console.warn(error);
      });
  }
  //endregion

  //region Handlers
  // Смена типа объявления
  handleTypeChange(e) {
    let currentType = e.target.value;

    this.setType(currentType);
  }

  // Смена типа предложения
  handleOfferTypeChange(e) {
    let offer_type = e.target.value;
    let workState;

    if (this.state.currentType === 'service')
      workState = Object.assign({}, this.state.workState, {type: offer_type});

    else
      workState = Object.assign({}, this.state.workState, {offer_type});

    this.setState({workState}, () => { this.checkValidation(); });
  }

  // Смена категории
  handleCategoryParamChange(e) {
    let categoryId = parseInt(e.target.value);

    if (this.state.currentType !== 'cargo') {
      const workState =
        Object.assign({}, this.state.workState, {category: categoryId});

      console.log(workState);

      this.setState({workState}, () => { this.checkValidation(); });
    }

    this.setState({categoryId}, () => { this.checkValidation(); });
  }
  //endregion

  //region Lifecycle

  // Действия после появления компонента в DOM дереве
  componentDidMount() {
    this.restoreWorkState();
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.step !== nextProps.step) {
      this.restoreWorkState();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    let updateDueToFetching = this.state.fetching !== nextState.fetching;
    let updateDueToValidity = this.state.isValid !== nextState.isValid;

    let shouldUpdate = updateDueToFetching || updateDueToValidity;

    return shouldUpdate;
  }

  //endregion

  //region Render

  render() {
    return (
      <section className={css.Step}>
        <div className={css.parameters}>
          {this.renderTypeSelect()}
          {this.renderOfferType() }
          {this.renderCategories()}
        </div>

        {this.props.edit ? null : this.renderNavigation()}

        <Notify ref="Notify"/>
      </section>
    );
  }

  // Отрисовка списка типов объявлений
  renderTypeSelect() {

    const types = [
      ['transport', 'Спецтехника'],
      ['equipment', 'Оборудование'],
      ['cargo'    , 'Грузы и грузоперевозки'],
      ['shop'     , 'Магазины и стройматериалы'],
      ['service'  , 'Услуги'],
      ['vacancy'  , 'Вакансии'],
      ['resume'   , 'Резюме']
    ];

    return(
      <div className={css.formGroup}>
        <label>Раздел</label>

        <select value={this.state.currentType} onChange={this.handleTypeChange.bind(this)}>
          <option value="none">Тип объявления</option>
          {
            types.map( type => <option key={type[0]} value={type[0]}>{type[1]}</option> )
          }
        </select>
      </div>
    );
  }

  // Отрисовка типа предложения
  renderOfferType() {
    let type = this.state.currentType;

    // Если тип объявления не выбран, не показываем компонент
    if (type === 'none') return null;

    let offerTypes = {
      transport: [
        {id: 1, type: 'rent', name: 'Аренда'},
        {id: 2, type: 'sell', name: 'Продажа'}
      ],
      equipment: [
        {id: 1, type: 'rent', name: 'Аренда'},
        {id: 2, type: 'sell', name: 'Продажа'}
      ],
      service: [
        {id: 1, type: 'order', name: 'Заказы'},
        {id: 2, type: 'offer', name: 'Предложения'}
      ],
    };

    // Показываем тип предложения только для транспорта, оборудования и грузов
    let isValidType = type === 'transport' || type === 'equipment' || type === 'service';

    if (isValidType) {
      let types = offerTypes[type] || [];

      return(
        <div className={css.formGroup}>
          <label>Тип</label>

          <select defaultValue={this.state.workState.offer_type} onChange={this.handleOfferTypeChange.bind(this)}>
            <option value="0">Тип предложения</option>
            {
              types.map( (offerType) => <option key={offerType.id} value={offerType.type}>{offerType.name}</option> )
            }
          </select>
        </div>
      );
    } else return null;
  }

  // Отрисовка категорий по типу объявления
  renderCategories() {
    let type = this.state.currentType;

    // Если тип объявления "груз" или не выбран, не показываем компонент
    if (type === 'cargo' || type === 'none' || type === undefined) return null;

    // Проверяем, загружены ли данные о категориях
    if (this.state.fetching) {

      return (
        <div className={css.formGroup}>
          <label>Категория</label>

          <select disabled>
            <option>Загрузка...</option>
          </select>
        </div>
      );

    } else {

      return (
        <div className={css.formGroup}>
          <label>Категория</label>

          <select value={this.state.categoryId} onChange={this.handleCategoryParamChange.bind(this)}>
            <option value="">Укажите категорию</option>
            {
              this.state.categories.map(
                param => <option key={'category_' + param.id} value={param.id}>{param.name}</option>
              )
            }
          </select>
        </div>
      );

    }
  }

  // Отрисовка кнопки "Далее"
  renderNavigation() {
    return(
      <nav>
        {
          this.state.isValid
            ? <button data-step="2" onClick={this.setStep.bind(this)}>Далее</button>
            : <button disabled>Укажите параметры</button>
        }
      </nav>
    );
  }

  //endregion
}
