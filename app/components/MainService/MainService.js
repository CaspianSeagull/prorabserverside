import React from 'react';
import {Link} from 'react-router';

import css from './style.styl';


/**
 *
 * @property {array} links - список объектов с данными для превью-ссылки
 *
 * @example
 * let array = [{
 *   url:    '/catalog/transport/rent',
 *   title:  'Техника',
 *   action: 'Арендовать',
 *   image:  '/media/popular/rent.jpg
 * }, {...}, {...}]
 */
export default class MainService extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      links: [
        {
          url: '/',
          title: 'Эвакуаторы',
          action: 'Заказать',
          image: '/img/photos/main/evacuators.png'
        },
        {
          url: '/',
          title: 'Бетономешаелки',
          action: 'Аренда',
          image: '/img/photos/main/betonomeshalki.png'
        },
        {
          url: '/',
          title: 'Песок',
          action: 'Купить',
          image: '/img/photos/main/sand.png'
        },
        {
          url: '/',
          title: 'Грузчики',
          action: 'Вызвать',
          image: '/img/photos/main/cargoman.png'
        },
        {
          url: '/',
          title: 'Грузоперевозка',
          action: 'Заказать',
          image: '/img/photos/main/delivery.png'
        },
        {
          url: '/',
          title: 'Маляр',
          action: 'Найти',
          image: '/img/photos/main/painter.png'
        },
      ]
    }
  }

  componentWillMount() {
    if (this.props.links) {
      this.setState({links: this.props.links})
    }
  }

  render() {
    return (
      <div className={css.MainService}>
        <div className="container">
          <div className="row">

            {
              this.state.links.map( linkItem =>
                <div key={`pop_${linkItem.title}`} className="col-xs-5 col-sm-4 col-md-2">
                  {renderPopularLink(linkItem)}
                </div>
              )
            }

          </div>
        </div>
      </div>
    );
  }
}

const renderPopularLink = (linkItem) =>
  <Link to={linkItem.url}>
    <div className={css.item}>
      <img src={linkItem.image} alt={linkItem.title}/>
      <div className={css.info}>
        <span>{linkItem.title}</span>
        <span>{linkItem.action}</span>
      </div>
    </div>
  </Link>
