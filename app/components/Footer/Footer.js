import React from 'react';
import {Link} from 'react-router';
import css   from './style.styl';

export default class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <footer className={css.Footer}>
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-sm-8">
              <nav>
                <Link to="/information/about">О сервисе</Link>
                <Link to="/information/feedback">Обратная связь</Link>
                <Link to="/information/agreement">Соглашения, оферта</Link>
                <Link to="/information/payment">Оплата и услуги</Link>
                <Link to="/information/support">Техподдержка</Link>
                <Link to="/information/job">Вакансии</Link>
                <Link to="/information/promote">Реклама на сайте</Link>
              </nav>

              <span>
                2016 ©  OOO "СтройСервис". Все права защищены. Использование сайта, в том числе подача объявлений, означает согласие с <a href="">пользовательским соглашением</a>. Оплачивая услуги на сайте, вы принимаете оферту.
              </span>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
