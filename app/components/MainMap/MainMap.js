import React     from 'react';
import GoogleMap from 'google-map-react';
import Cluster   from 'points-cluster';
import axios     from 'axios';

import API from '../../API';
import {Placemark, MapCluster} from '../Placemark/Placemark';
import css from './style.styl';


/**
 * Компонент карты с метками объявлений
 * 
 * @export
 * @class MainMap
 * @extends {React.Component}
 * 
 * @example
 * <MainMap category="transport"/>
 */
export default class MainMap extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mapParameters: {
        key: 'AIzaSyA1l_CX9YVk3O_qywJUaVT0RIiRK49AFQw',
        language: 'ru'
      },
      fetching: true,
      category: 'transport',
      center: {lat: 42, lng: 47},
      markers: [],
      clusters: [],
      zoom: 10
    };
  }

  /** Обработка изменений на карте.
   * Запись значения зума и вызов кластеризации
   * @memberOf MainMap
   * 
   * @param {object} map - объект измененных данных с карты
   */
  handleMapChange(map) {
    this.setState({
      zoom: map.zoom
    }, () => this.setClusters());
  }

  /** Загрузка меток с сервера
   * Запись меток в состояние компонента и вызов кластеризации
   * @memberOf MainMap
   * 
   * @param {string} category - категория объявлений
   */
  fetchMarkers(category) {
    this.setState({fetching: true});
    axios.get(API.Markers.type(category, this.props.filterParams))
      .then(
        response => {
          let markers = response.data;

          this.setState({
            markers,
            category: this.props.category,
            fetching: false
          }, () => {
            this.setClusters();
          });
        }
      ).catch(error => console.warn(error));
  }

  /** Запись массива кластеров в состояние компонента
   * @memberOf MainMap
   */
  setClusters() {
    let clusters = this.getClustersFromMarkers();
    this.setState({clusters});
  }

  /** Кластеризация из массива меток в состоянии компонента
   * @memberOf MainMap
   * @returns {Array} cluster - массив кластеров
   */
  getClustersFromMarkers() {
    let locations = this.state.markers.map( item => {
      return {
        lat: item.location.lat,
        lng: item.location.lon
      };
    });

    let clusterer = Cluster(locations, {minZoom: 1, maxZoom: 20, radius: 60});
    return clusterer({ bounds: { nw: { lat: 85, lng: -180 }, se: { lat: -85, lng: 180 } }, zoom: this.state.zoom });
  }

  componentDidMount() {
    let location = localStorage.getItem('userLocation');
    if (location) {
      this.setState({
        center: JSON.parse(location)
      });
    }

    this.fetchMarkers(this.props.category);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.category !== this.state.category) {
      this.fetchMarkers(nextProps.category);
    }
  }
  render() {
    let key = 0;

    return (
      <div className={css.MainMap}>
        <GoogleMap
          center={this.state.center}
          zoom={10}
          onChange={this.handleMapChange.bind(this)}
          bootstrapURLKeys={this.state.mapParameters}
        >
        {
          this.state.fetching ? '' :
            this.state.clusters.map( clusterPoint => this.renderClusterPoint(clusterPoint, this.state.category, key++) )
        }
        </GoogleMap>
      </div>
    );
  }


  /** Отрисовка кластеров и меток на карту
   * @memberOf MainMap
   * 
   * @param {object} clusterPoint - Объект метки с параметрами широты и долготы
   * @param {string} category     - Тип объявления метки
   * @param {number} key          - Ключ элемента для реакта
   * 
   * @return {React.Component} Placemark  - метка одиночного объявления
   * @return {React.Component} MapCluster - метка кластера
   */
  renderClusterPoint(clusterPoint, category, key) {
    if (clusterPoint.numPoints === 1) {
      return <Placemark
        category={category}
        key={`placemark_${category}_${key}`}
        lat={clusterPoint.y}
        lng={clusterPoint.x}
      />;
    } else {
      return <MapCluster
        category={category}
        key={`cluster_${category}_${key}`}
        lat={clusterPoint.y}
        lng={clusterPoint.x}
        amount={clusterPoint.numPoints}
      />;
    }
  }
}
