export const Rus = {
  transport: 'Спецтехника',
  equipment: 'Строительное оборудование',
  cargo: 'Грузы и грузоперевозки',
  shop: 'Магазины и стройматериалы',
  service: 'Услуги',
  vacancy: 'Ваканси',
  resume: 'Резюме'
};

export const RusShort = {
  transport: 'Спецтехника',
  equipment: 'Оборудование',
  cargo: 'Грузы',
  shop: 'Магазины',
  service: 'Услуги',
  vacancy: 'Ваканси',
  resume: 'Резюме'
};

export const RusShortGenitive = {
  transport: 'Спецтехники',
  transports: 'Спецтехники',
  equipment: 'Оборудования',
  equipments: 'Оборудования',
  cargo: 'Грузов',
  cargoes: 'Грузов',
  shop: 'Магазинов',
  shops: 'Магазинов',
  service: 'Услуг',
  services: 'Услуг',
  vacancy: 'Вакансий',
  vacancies: 'Вакансий',
  resume: 'Резюме',
  resumes: 'Резюме'
};

export const RusMulty = {
  transport: 'Спецтехника',
  transports: 'Спецтехника',
  equipment: 'Строительное оборудование',
  equipments: 'Строительное оборудование',
  cargo: 'Грузы и грузоперевозки',
  cargoes: 'Грузы и грузоперевозки',
  shop: 'Магазины и стройматериалы',
  shops: 'Магазины и стройматериалы',
  service: 'Услуги',
  services: 'Услуги',
  vacancy: 'Ваканси',
  vacancies: 'Ваканси',
  resume: 'Резюме',
  resumes: 'Резюме'
};

