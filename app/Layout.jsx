import React from 'react';
import {connect} from 'react-redux';


class Layout extends React.Component {

    render() {
        const custom = this.props.custom;
        return (
            <html>
                <head>
                    <title>{custom.title}</title>
                    <link rel='stylesheet' href='/bundle.css' />
                </head>
                <body>
                    {this.props.children}
                    <script dangerouslySetInnerHTML={{
                        __html: 'window.PROPS=' + JSON.stringify(custom)
                    }} />
                    <script src='/bundle.js' />
                </body>
            </html>
        );
    }

}

const wrapper = connect(
    function(state) {
        return { custom: state };
    }
);

module.exports = wrapper(Layout);
