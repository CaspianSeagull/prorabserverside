export const SET_STATUS_READY = 'SET_STATUS_READY';
export const SET_STATUS_FETCHING = 'SET_STATUS_FETCHING';
export const SET_USER_INFO = 'SET_USER_INFO';
export const AUTHORIZE = 'AUTHORIZE';
export const LOG_OUT = 'LOG_OUT';
export const SET_COORDINATES = 'SET_COORDINATES';
export const SET_CITY = 'SET_CITY';