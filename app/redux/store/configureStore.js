import {createStore, applyMiddleware} from 'redux';

import rootReducer from '../reducers';
import thunk       from 'redux-thunk';

// Создание store из дефолтного редьюсера
export default function configureStore(preloadedState) {
  const store = createStore(
    rootReducer,
    preloadedState,

    // Thunk для возврата асинхронных функций вместо action creators
    applyMiddleware(thunk)
  );

  // Обновление данных для hot reload
  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default;
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}
