import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import expect from 'expect';

import API, {globalDomain as Domain} from './API';

import * as Ads from '../actions/AdsActions';
import * as AdsTypes from '../constants/AdsActionTypes';

import * as App from '../actions/AppActions';
import * as AppTypes from '../constants/AppActionTypes';

import * as Bm from '../actions/BookmarkActions';
import * as BmTypes from '../constants/BookmarkActionTypes';

import * as User from '../actions/UserActions';
import * as UserTypes from '../constants/UserActionTypes';

const middlewares = [ thunk ];
const mockStore = configureMockStore(middlewares);

describe('[REDUX:ACTIONS] Ads',  () => {

  afterEach(() => {nock.cleanAll();});

  it('Запись объявлений в Store', () => {
    const data = [{id: 1}, {id: 2}, {id: 3}];

    const expectedAction = {
      type: AdsTypes.SET_ADS_LIST,
      status: 'ready',
      list: data
    };

    expect( Ads.setAdsList(data) ).toEqual(expectedAction);
  });

  it('Установка параметров фильтрации', () => {
    const data = {param1: 1, param2: '2'};

    const expectedAction = {
      type: AdsTypes.SET_FILTER_PARAMS,
      params: data
    };

    expect( Ads.setFilterParams(data) ).toEqual(expectedAction);
  });

  it('Установка статуса ожидания ответа', () => {
    const expectedAction = {
      type: AdsTypes.SET_ADS_FETCHING,
      status: 'fetching'
    };

    expect( Ads.setAdsFetch('fetching') ).toEqual(expectedAction);
  });

  it('Загрузка массива объявлений с сервера', () => {
    nock(Domain)
      .get( API.Ads.items('transport', 2, {param: 10}) )
      .reply(200, [{id: 1}, {id: 2}, {id: 3}]);

    const expectedActions = [
      {
        type: AdsTypes.SET_ADS_FETCHING,
        status: 'fetching'
      },
      {
        type: AdsTypes.SET_ADS_LIST,
        status: 'ready',
        list: [{id: 1}, {id: 2}, {id: 3}]
      }
    ];

    const store = mockStore({ CurrentAdsList: [] });

    return store.dispatch(Ads.setAdsListAsync('transport', 2, {param: 10}))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });

  });

});

describe('[REDUX:ACTIONS] App',  () => {

  it('Включение карты', () => {
    const expectedAction = {type: AppTypes.SET_MAP_ACTIVE};

    expect( App.enableMap() ).toEqual(expectedAction);
  });

  it('Выключение карты', () => {
    const expectedAction = {type: AppTypes.SET_MAP_INACTIVE};

    expect( App.disableMap() ).toEqual(expectedAction);
  });

});

describe('[REDUX:ACTIONS] User', () => {

  afterEach(() => {nock.cleanAll();});

  it('Запись координат пользователя', () => {
    const coords = {lat: 3.14, lng: 42};

    const expectedAction = {
      type: UserTypes.SET_COORDINATES,
      lat: coords.lat,
      lng: coords.lng
    };

    expect( User.setUserCoords(coords.lat, coords.lng) ).toEqual(expectedAction);
  });

  it('Запись данных пользователя в Store и localStorage', () => {
    let user = {first_name: 'John Doe'};

    const expectedAction = {
      type: UserTypes.SET_USER_INFO,
      userInfo: user
    };

    expect( User.setUserInfo(user) ).toEqual(expectedAction);

    let localUser = localStorage.getItem('userInfo');

    expect( JSON.parse(localUser) ).toEqual(user);
  });

  it('Верификация юзера, запись токена и данных', () => {

    const verifyData = {
      phone: '+79891234567',
      passcode: 12345
    };

    let user = {first_name: 'John Doe'};

    nock(Domain)
      .post(API.Accounts.verify, verifyData)
      .reply(200, {Token: 'faketoken'});

    nock(Domain)
      .get(API.Accounts.users(1))
      .reply(200, user);

    const expectedActions = [
      {
        type: UserTypes.SET_STATUS_FETCHING
      },
      {
        type: UserTypes.SET_USER_INFO,
        userInfo: user
      }
    ];

    const store = mockStore({ info: null, fetching: 'fetching' });

    return store.dispatch(User.verifyUser(verifyData.phone, verifyData.passcode))
      .then(() => {
        // TODO: Разобраться с тестом
        // expect(store.getActions()).toEqual(expectedActions);

        // Проверка записи токена в localStorage
        let token = localStorage.getItem('clientToken');
        expect(token).toEqual('faketoken');

        // Проверка записи информации о пользователе в localStorage
        let localUser = localStorage.getItem('userInfo');
        expect(JSON.parse(localUser)).toEqual(user);
      });

  });

});

describe('[REDUX:ACTIONS] Bookmarks', () => {

  afterEach(() => {nock.cleanAll();});

  it('Запись списка закладок', () => {
    const bms = [{id: 1}, {id: 2}];

    const expectedAction = {
      type: BmTypes.SET_BOOKMARK_LIST,
      list: bms
    };

    expect( Bm.setBookmarkList(bms)).toEqual(expectedAction);
  });

  it('Установка статуса соединения с сервером', () => {

    const trueAct  = {
      type: BmTypes.SET_BOOKMARK_FETCH,
      isFetching: true
    };
    const falseAct = {
      type: BmTypes.SET_BOOKMARK_FETCH,
      isFetching: false
    };

    expect( Bm.fetchingBookmarks(true)).toEqual(trueAct);
    expect( Bm.fetchingBookmarks(false)).toEqual(falseAct);

  });

  it('Загрузка закладок с сервера', () => {

    let bookmarks = [{id: 1}, {id: 2}];

    nock(Domain, {reqheaders: {'Authorization': 'Token faketoken'}})
      .get(API.Bookmarks.items)
      .reply(200, bookmarks);

    const expectedActions = [
      {
        type: BmTypes.SET_BOOKMARK_FETCH,
        isFetching: true
      },

      {
        type: BmTypes.SET_BOOKMARK_LIST,
        list: bookmarks
      }
    ];

    const store = mockStore({list: [], fetching: false});

    return store.dispatch( Bm.fetchBookmarks('faketoken') )
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });

});