import expect from 'expect';

import ads, {initialState as AdsInit} from '../reducers/ads';
import * as AdsTypes from '../constants/AdsActionTypes';

import app, {initialState as AppInit} from '../reducers/app';
import * as AppTypes from '../constants/AppActionTypes';

import user, {initialState as UserInit} from '../reducers/user';
import * as UserTypes from '../constants/UserActionTypes';

import bookmark, {initialState as BmInit} from '../reducers/bookmark';
import * as BmTypes from '../constants/BookmarkActionTypes';

describe('[REDUX:REDUCERS] Initial states',  () => {

  it('Создание первичных состояний редьюсеров', () => {
    expect( ads(undefined, {}) ).toEqual(AdsInit);
    expect( app(undefined, {}) ).toEqual(AppInit);
    expect( user(undefined, {}) ).toEqual(UserInit);
    expect( bookmark(undefined, {}) ).toEqual(BmInit);
  });

});