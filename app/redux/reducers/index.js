import { combineReducers } from 'redux';

import app      from './app';
import ads      from './ads';
import user     from './user';
import bookmark from './bookmark';

const rootReducer = combineReducers({
  app,
  ads,
  user,
  bookmark
});

export default rootReducer;