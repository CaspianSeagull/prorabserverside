import {
  SET_ADS_LIST,
  SET_ADS_FETCHING,
  SET_FILTER_PARAMS
} from '../constants/AdsActionTypes';


export const initialState = {
  CurrentAdsList: null,
  fetching: 'fetching',
  filterParams: null
};


export default function ads(state = initialState, action) {
  switch (action.type) {

    case SET_ADS_LIST: {
      return Object.assign({}, state, {
        CurrentAdsList: action.list,
        fetching: action.status
      });
    }

    case SET_ADS_FETCHING: {
      return Object.assign({}, state, {
        fetching: action.status
      });
    }

    case SET_FILTER_PARAMS: {
      return Object.assign({}, state, {
        filterParams: action.params
      });
    }

    default:
      return state;
  }
}
