import {
  SET_BOOKMARK_LIST,
  SET_BOOKMARK_FETCH
} from '../constants/BookmarkActionTypes';


export const initialState = {
  list: [],
  fetching: false
};


export default function app(state = initialState, action) {
  switch (action.type) {

    case SET_BOOKMARK_FETCH: {
      return Object.assign({}, state, {
        fetching: action.isFetching
      });
    }

    case SET_BOOKMARK_LIST: {
      return Object.assign({}, state, {
        list: action.list,
        fetching: false
      });
    }

    default:
      return state;
  }
}
