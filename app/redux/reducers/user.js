import {
  SET_STATUS_READY,
  SET_STATUS_FETCHING,
  SET_USER_INFO,
  AUTHORIZE,
  LOG_OUT,
  SET_COORDINATES,
  SET_CITY
} from '../constants/UserActionTypes';


/** State модели пользователя
 *
 * @property {object} info - объект с информацией о пользователе
 * @property {boolean} authorized - состояние авторизации пользователя
 * @property {string} fetching - состояние обращения к серверу
 * @property {{lat: number, lng: number}} - координаты пользователя
 * @property {string} - строка с названием Города, полученного из координат
 */
export const initialState = {
  info: null,
  authorized: false,
  fetching: 'fetching',
  coords: {lat: 0, lng: 0},
  city: 'Определение города...',
  state: ''
};

/** Редьюсер модели пользователя
 *
 * @param {object} state - текущий state
 * @param {object} action - объект с измененными частями state
 *
 * @returns {object} newState - новое состояние части приложения
 */
export default function user(state = initialState, action) {
  switch (action.type) {

    case SET_STATUS_READY: {
      return Object.assign({}, state, {
        fetching: 'ready'
      });
    }

    case SET_STATUS_FETCHING: {
      return Object.assign({}, state, {
        fetching: 'fetching'
      });
    }

    case SET_USER_INFO: {
      return Object.assign({}, state, {
        info: action.userInfo,
        fetching: 'ready',
        authorized: true
      });
    }

    case AUTHORIZE: {
      return Object.assign({}, state, {
        authorized: true
      });
    }

    case LOG_OUT: {
      return Object.assign({}, state, {
        authorized: false,
        info: null
      });
    }

    case SET_COORDINATES: {
      return Object.assign({}, state, {
        coords: {lat: action.lat, lng: action.lng}
      });
    }

    case SET_CITY: {
      return Object.assign({}, state, {
        city: action.city
      });
    }

    default:
      return state;
  }
}
