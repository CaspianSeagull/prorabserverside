import {
  SET_MAP_ACTIVE,
  SET_MAP_INACTIVE,
  SET_MENU_ACTIVE,
  SET_MENU_INACTIVE,
  SET_CURRENT_CATEGORY
} from '../constants/AppActionTypes';


export const initialState = {
  mapActive: false,
  currentCategory: 'transport',
  menuActive: true
};


export default function app(state = initialState, action) {
  switch (action.type) {

    case SET_MAP_ACTIVE: {
      return Object.assign({}, state, {
        mapActive: true
      });
    }

    case SET_MAP_INACTIVE: {
      return Object.assign({}, state, {
        mapActive: false
      });
    }

    case SET_MENU_ACTIVE: {
      return Object.assign({}, state, {
        menuActive: true
      });
    }

    case SET_MENU_INACTIVE: {
      return Object.assign({}, state, {
        menuActive: false
      });
    }

    case SET_CURRENT_CATEGORY: {
      return Object.assign({}, state, {
        currentCategory: action.category
      });
    }

    default:
      return state;
  }
}
