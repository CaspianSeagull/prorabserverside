/** Redux actions для работы с объявлениями
 * @class ReduxAds
 */

import * as type from '../constants/AdsActionTypes';
import axios from 'axios';
import API   from '../../API';

import fetch from 'isomorphic-fetch';

/** Записываем массив объектов объявлений в Store
 *
 * @param list - массив объявлений
 * @return {{type: string, status: string, list: array}}
 */
export function setAdsList(list) {
  return {
    type: type.SET_ADS_LIST,
    status: 'ready',
    list
  };
}

/** Указываем состояние соединения с сервером
 *
 * @param {string} status
 * @return {{type: string, status: string}}
 */
export function setAdsFetch(status) {
  return {
    type: type.SET_ADS_FETCHING,
    status
  };
}

/** Запись последнего состояния фильтрации в Store
 *
 * @param {object} params - список параметров
 * @return {{type: string, params: object}}
 */
export function setFilterParams(params) {
  return {
    type: type.SET_FILTER_PARAMS,
    params
  };
}

/** Асинхронная загрузка объявлений в Store приложения
 * @memberOf ReduxAds
 *
 * @param {string} type - тип объявления
 * @param {number} page - номер страницы
 * @param {object} filterParams - JSON объект с параметрами фильтрации
 * @param {boolean} all - указывает, нужно ли грузить все объявления (нужно для объявлений юзера, по сути лимит на 9999)
 *
 * @example
 * setAdsListAsync("transport", 3, {price_0: 4000, category: 3});
 *
 * @return {function(*)} - promise function
 */
export function setAdsListAsync(type, page = 1, filterParams, all = false) {
  return (dispatch) => {
    dispatch( setAdsFetch('fetching') );

    return fetch(API.Ads.items(type, page, filterParams, {all}))
      .then(  res   => res.json() )
      .then(  json  => dispatch(setAdsList(json)) )
      .catch( error => console.warn(error) );
  };
}