import * as type from '../constants/UserActionTypes';

import axios from 'axios';
import API   from '../../API';

import fetch from 'isomorphic-fetch';


export function setStatusReady() {
  return {
    type: type.SET_STATUS_READY
  };
}

export function setStatusFetching() {
  return {
    type: type.SET_STATUS_FETCHING
  };
}

export function authorize() {
  return {
    type: type.AUTHORIZE,
  };
}

export function logOut() {
  localStorage.removeItem('clientToken');

  return {
    type: type.LOG_OUT,
  };
}

export function setUserInfo(userInfo) {

  localStorage.setItem('userInfo', JSON.stringify(userInfo));

  return {
    type: type.SET_USER_INFO,
    userInfo
  };
}

export function setUserCoords(lat, lng) {
  return {
    type: type.SET_COORDINATES,
    lat,
    lng
  };
}

export function setUserCity(city) {
  return {
    type: type.SET_CITY,
    city
  };
}


/**
 * Обновляем данные пользователя
 *
 * @param {number} userId - id пользователя
 * @param {object} userInfo - объект с обновленными данными
 * @param {string} token - токен авторизации
 *
 * @returns {function(*)} - промис для Redux-thunk
 *
 * @example
 * updateUserInfo(3, {phone: '+79891234567', email: 'test@mail.ru'}, '233t2dip2uyt0f928y5n')
 */
export function updateUserInfo(userId, userInfo, token) {

  let validUserInfo = userInfo.phone && userInfo.first_name && userInfo.last_name;

  if (token && validUserInfo) {
    return dispatch => {
      dispatch( setStatusFetching() );

      delete userInfo.avatar;

      axios({
        method: 'PUT',
        url: API.Accounts.users(userId),
        headers: {
          'Authorization': `Token ${token}`
        },
        data: userInfo
      })
      .then( response => {
        dispatch(setUserInfo(response.data));
      })
      .catch( error => console.warn(error) );

    };
  }
}

export function updateUserPhoto(userId, file, token, fetchCallback) {
  if (token) {
    return dispatch => {
      dispatch( setStatusFetching() );

      axios({
        method: 'PATCH',
        url: API.Accounts.users(userId),
        headers: {
          'Authorization': `Token ${token}`
        },
        data: file
      })
      .then( (response) => {
        dispatch(setUserInfo(response.data));
        fetchCallback(false);
      })
      .catch(err => {
        fetchCallback(false);
        console.warn(err);
      });
    };
  }

}

// Получаем данные пользователя по токену и записываем в store
export function fetchUserData(token) {

  return dispatch => {
    dispatch( setStatusFetching() );

    axios
      .get(API.Accounts.user, {headers: {'Authorization': `Token ${token}`}})
      .then( response => {
        let UserID = response.data.id;

        axios
          .get(API.Accounts.users(UserID))
          .then( response => {
            dispatch(setUserInfo(response.data));
          });

      })
      .catch( error => console.warn(error) );

  };
}

// Получаем токен пользователя по номеру телефона и коду подтверждения
export function verifyUser(phone, passcode) {
  return dispatch => {

    const postData = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({phone, passcode})
    };

    return fetch(API.Accounts.verify, postData)
      .then( res => res.json())
      .then( json => {
        let newToken = json.Token;

        if (newToken) {
          localStorage.setItem('clientToken', newToken);
        }

        return dispatch(fetchUserData(newToken));
      })
      .catch( error => console.warn(error) );
  };
}