import * as type from '../constants/BookmarkActionTypes';

import axios from 'axios';
import API from '../../API';

import fetch from 'isomorphic-fetch';

export function setBookmarkList(list) {
  return {
    type: type.SET_BOOKMARK_LIST,
    list
  };
}

export function fetchingBookmarks(isFetching) {
  return {
    type: type.SET_BOOKMARK_FETCH,
    isFetching
  };
}


export function fetchBookmarks(token) {
  if (token) {
    return function(dispatch) {
      dispatch(fetchingBookmarks(true));

      const headers = {
        headers: {
          'Authorization': `Token ${token}`
        }
      };

      return fetch(API.Bookmarks.items, headers)
        .then(  res   => res.json())
        .then(  json  => dispatch(setBookmarkList(json)) )
        .catch( error => console.warn(error));
    };
  }

  else console.warn('Wrong token:', token);
}

export function addBookmark(ad_type, ad_id, token) {
  return function(dispatch) {
    dispatch(fetchingBookmarks(true));

    axios
      .post(API.Bookmarks.items, {ad_type, ad_id}, {headers: {
        'Authorization': `Token ${token}`,
        'Content-Type': 'application/json'
      }})
      .then(
        (response) => {
          dispatch( fetchBookmarks(token) );
        }
      )
      .catch(error => console.warn(error));
  };
}

export function removeBookmark(bm_id, token) {
  return function(dispatch) {
    dispatch(fetchingBookmarks(true));

    return axios
      .delete(API.Bookmarks.item(bm_id), {headers: {'Authorization': `Token ${token}`}})
      .then(
        (response) => {
          dispatch( fetchBookmarks(token) );
        }
      )
      .catch(error => console.warn(error));
  };
}
