import * as type from '../constants/AppActionTypes';

export function enableMap() {
  return {
    type: type.SET_MAP_ACTIVE,
  };
}

export function disableMap() {
  return {
    type: type.SET_MAP_INACTIVE,
  };
}