import React from 'react';
import axios from 'axios';
import API from '../../API';
import Menu from '../../components/Menu/Menu';
import Step1 from '../../components/CreateSteps/Step1.jsx';
import Step2 from '../../components/CreateSteps/Step2.jsx';
import Step3 from '../../components/CreateSteps/Step3.jsx';
import Final from '../../components/CreateSteps/Final.jsx';
import css   from './style.styl';

/** Обертка для создания нового объявления
 * @property {number} step - текущий шаг создания/редактирования объявления
 */
export default class Create extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      /** Текущий шаг создания объявления */
      step: 1,
      adInfo: null,
      editId: null,
      edit: false
    };
  }

  // Меняем параметры из существующей модели в вид для отправки на сервер
  transformEditParams(oldParams) {
    const newParams = {};

    oldParams.forEach( (param) => {
      newParams[param.slug] = param.value;
    });

    return newParams;
  }

  setBackupFromItem(item) {
    let currentType = this.props.params.type;
    let categoryId = null;

    switch(currentType) {
      case 'transport':
      case 'equipment':
        categoryId = item.category.id;
        break;

      default:
        categoryId = item.category;
        break;
    }

    let newParams, fixParamsItem;

    if (currentType === 'transport' || currentType === 'equipment' || currentType === 'service') {
      newParams = this.transformEditParams(item.parameters);
      fixParamsItem = Object.assign({}, item, {parameters: newParams});    
    } else {
      fixParamsItem = item;
    }

    const newAdBackup = {
      currentType,
      categoryId,
      workState: fixParamsItem
    };

    localStorage.setItem('newAdBackup', JSON.stringify(newAdBackup));
    localStorage.setItem('newAdPhotos', JSON.stringify(item.images));
  }

  fetchAdInfo(type, id) {
    axios
      .get( API.Ads.item(type, id) )
      .then( response => {

        this.setBackupFromItem(response.data);

        this.setState({
          editId: this.props.params.id,
          edit: true,
          editItem: response.data
        });
      })
      .catch(error => console.warn(error));
  }

  /** Устанавливаем текущий шаг
   * @param {number} step - шаг создания объявления
   */
  setStep(step) {
    this.setState({step});
  }

  componentDidMount() {
    if (this.props.route.edit) {
      this.fetchAdInfo(this.props.params.type, this.props.params.id);
    }
  }



  /** Отрисовка компонента */
  render() {
    return (
      <div className={css.Create}>

        <Menu
          currentMenu={'info'}
          isHidden={true}
        />

        <div className="container">
          <div className="row">

            <div className="col-xs-12">
              <h1 className={css.innerTitle}>{this.props.edit ? 'Редактирование' : 'Создание нового'} объявления</h1>
            </div>

            <div className="col-xs-12 col-sm-4">
              {this.renderNavigation()}
            </div>

            <div className="col-xs-12 col-sm-8">
              {renderTitle(this.props.edit, this.state.step)}

              {this.renderSteps()}
            </div>

          </div>
        </div>
      </div>
    );
  }

  // Рисуем боковое меню
  renderNavigation() {
    const steps = ['Категория', 'Параметры', 'Контакты', 'Завершение'];

    return(
      <ul className={css.steps}>
        {
          steps.map( (step, i) =>
            <li key={'step_' + i}
              className={this.state.step >= i+1 ? css.active : null}
            >
              {i+1 + '. ' + step}
            </li>
          )
        }
      </ul>
    );
  }

  // Показываем шаги
  renderSteps() {
    let stepComponent = null;

    const stepsParams = {
      edit: this.state.edit,
      editId: this.state.editId,
      step: this.state.step,
      setStep: this.setStep.bind(this)
    };

    if (this.state.edit) {

      switch(this.state.step) {

        case 4:
          stepComponent = <Final {...stepsParams}/>;
          break;

        default:
          stepComponent =
            <div>
              <Step1 {...stepsParams}/>
              <Step2 {...stepsParams}/>
              <Step3 {...stepsParams}/>
            </div>;
          break;

      }

    } else {

      switch(this.state.step) {
        case 1:
          stepComponent = <Step1 {...stepsParams}/>;
          break;

        case 2:
          stepComponent = <Step2 {...stepsParams}/>;
          break;

        case 3:
          stepComponent = <Step2 {...stepsParams}/>;
          break;

        case 4:
          stepComponent = <Final {...stepsParams}/>;
          break;

        default: break;
      }

    }

    return stepComponent;

  }
}


const renderTitle = (edit, step) => {

}