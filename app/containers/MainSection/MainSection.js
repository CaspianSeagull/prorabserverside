import React from 'react';
import MainLinks   from '../../components/MainLinks/MainLinks';
import css from './style.styl';

export default class MainSection extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section className={css.main}>
        <div className="container">
          <h1>Ваш карманный прораб</h1>

          <MainLinks />
        </div>


      </section>
    );
  }
}