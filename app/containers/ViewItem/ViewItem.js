import React from 'react';
import axios from 'axios';

import MobilePromo from '../../components/MobilePromo/MobilePromo';
import AdActions   from '../../components/AdActions/AdActions';
import ViewAds     from '../../components/ViewAds/ViewAds';
import Loading     from '../../components/Loading/Loading';

import * as API from '../../API';

import css from './style.styl';

export default class ViewItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      item: null
    };
  }

  componentDidMount() {
    this.fetchAdInfo(this.props.params);
  }

  componentWillReceiveProps(nextProps) {
    this.fetchAdInfo(nextProps.params);
  }

  fetchAdInfo(params) {
    this.setState({loading: true});

    axios
      .get(API.Ads.item(params.category, params.id))
      .then( response => this.setState({item: response.data, loading: false}) )
      .catch(error => console.warn(error));
  }

  render() {
    return (
      <div className={css.ViewItem}>
        <div className="container">
          <div className="row">

            <div className="col-md-9">
              {
                this.state.loading ? <Loading /> : 
                  <ViewAds
                    category={this.props.params.category}
                    id={this.props.params.id}
                    item={this.state.item}
                  />
                
              }
            </div>

            <div className="col-md-3">
              <AdActions />
              <MobilePromo />
            </div>

          </div>
        </div>
      </div>
    );
  }
}
