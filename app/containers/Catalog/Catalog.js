import React from 'react';
import Menu  from '../../components/Menu/Menu';
import css   from './style.styl';
import Helmet from 'react-helmet';
import {RusMulty} from '../../types_local';

export default class Catalog extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {
    let newPage = this.props.params.page
  }

  shouldComponentUpdate(nextProps, nextState) {
    let
      newCategory = this.props.params.category !== nextProps.params.category,
      newAction   = this.props.params.action   !== nextProps.params.action,
      newPage     = this.props.params.page     !== nextProps.params.page;

    let shouldUpdate = newCategory || newAction || newPage;

    return shouldUpdate;
  }

  render() {
    let isNotView      = this.props.location.pathname.search(/view/) === -1;
    let isNotBookmarks = this.props.location.pathname.search(/bookmarks/) === -1;
    let visible = isNotView && isNotBookmarks;



    return (
      <div className={css.Catalog}>
        {this.getPageMeta()}
        <Menu
          currentMenu={this.props.params.category}
          currentType={this.props.params.action}
          isHidden={!visible}
        />

        {this.props.children}
      </div>
    );
  }

  getPageMeta() {
    let rusCat = RusMulty[this.props.params.category];

    const meta = {
      title: rusCat,
      meta: [
        {name: 'description', content: `Объявления в категории ${rusCat}`},
        {name: 'keywords', content: 'техника, аренда, техники, стройтехника, строительная техника, объявления, продажа, продать машину'}
      ],
      link: [
        {rel: 'canonical', href: `http://pro-rab.ru/catalog/${this.props.params.category}/${this.props.params.action}`}
      ]
    };

    return <Helmet {...meta} />;
  }
}