import React from 'react';

import css from './style.styl';

export default class NotFound extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={css.NotFound}>
        <h1>Страница не найдена</h1>
      </div>
    );
  }
}
