import React from 'react';
import axios from 'axios';

import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import * as AdsActions      from '../../redux/actions/AdsActions';
import * as AppActions      from '../../redux/actions/AppActions';
import * as BookmarkActions from '../../redux/actions/BookmarkActions';

import SideBar    from '../../components/SideBar/SideBar';
import Pagination from '../../components/Pagination/Pagination';
import MainMap    from '../../components/MainMap/MainMap';
import Preview    from '../../components/Preview/Preview/Preview.jsx';
import AdsPreview from '../../components/Preview/AdsPreview/AdsPreview';

import API from '../../API';

import css from './style.styl';

class AdsList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: 0,
      currentCategory: '',
      currentAction: '',
      categoryTypes: []
    };
  }

  //region Handlers
  handleCategoryTypeChange(e) {
    this.props.AdsActions.setAdsFetch('fetching');

    let type_id = e.target.value;
    let currentFilter = this.props.ads.filterParams || {};

    const newFilter = Object.assign({}, currentFilter, {
      category: type_id || ''
    });

    this.props.AdsActions.setFilterParams(newFilter);

    this.fetchAdsData(newFilter);
  }
  //endregion

  //region Fetching
  fetchCategoryTypes(category) {
    switch(category) {
      case 'transport':
        axios
          .get(API.Transport.types.categories)
          .then(response => {
            this.setState({
              categoryTypes: response.data
            });
          });
        break;

      case 'equipment':
        axios
          .get(API.Equipment.types.categories)
          .then(response => {
            this.setState({
              categoryTypes: response.data
            });
          });
        break;

      case 'shops':
        axios
          .get(API.Shop.categories)
          .then(response => {
            this.setState({
              categoryTypes: response.data
            });
          });
        break;

      case 'service':
        axios
          .get(API.Service.categories)
          .then(response => {
            this.setState({
              categoryTypes: response.data
            });
          });
        break;

      case 'vacancy':
      case 'resume':
        axios
          .get(API.Job.categories)
          .then(response => {
            this.setState({
              categoryTypes: response.data
            });
          });
        break;

      default:
        break;
    }
  }
  fetchAdsData(filterParams) {
    this.props.AdsActions.setAdsListAsync(
      this.props.params.category,
      this.state.currentPage,
      filterParams || this.props.ads.filterParams
    );
  }
  //endregion

  //region Filters
  filterReset(e) {
    e.preventDefault();

    let newFilter = Object.assign({}, {
      offer_type: this.props.params.action
    });

    this.props.AdsActions.setFilterParams(newFilter);
  }
  filterByAction(props) {
    this.setOfferTypeFilter(props.params.category, props.params.action);

    this.props.AdsActions.setAdsFetch('fetching');

    let page = parseInt(props.params.page) || 1;
    if (page < 1) page = 1;

    // Записываем номер текущей страницы и категорию в состояние компонента
    this.setState({
      currentPage: page,
      currentCategory: props.params.category
    }, () => {
      this.fetchAdsData();
    });
  }
  setOfferTypeFilter(category, value) {
    let currentFilter, newFilter;

    switch(category) {
      case 'transport':
      case 'equipment':
        currentFilter = this.props.ads.filterParams || {};

        newFilter = Object.assign({}, currentFilter, {
          offer_type: value
        });

        this.props.AdsActions.setFilterParams(newFilter);
        break;


      case 'service':
        currentFilter = this.props.ads.filterParams || {};

        newFilter = Object.assign({}, currentFilter, {
          type: value
        });

        this.props.AdsActions.setFilterParams(newFilter);
        break;


      default:
        this.props.AdsActions.setFilterParams({});
        break;
    }
  }
  //endregion

  //region Data operations
  addBookmark(ad_type, ad_id) {
    let token = localStorage.getItem('clientToken');
    this.props.BookmarkActions.addBookmark(ad_type, ad_id, token);
  }
  removeBookmark(ad_type, ad_id) {
    let token = localStorage.getItem('clientToken');
    let bmByAdId = this.props.bookmark.list.filter( item => item.ad !== null && item.ad.id === ad_id && item.ad_type === ad_type );

    this.props.BookmarkActions.removeBookmark(bmByAdId[0].id, token);
  }
  buildBmIdList(bookmarks) {
    let types = ['transport', 'equipment', 'cargoes', 'shops', 'service', 'resume', 'vacancy'];
    const bms = {};

    types.forEach( (type) => {
      let typeList = bookmarks.filter(bookmark => bookmark.ad_type === type );

      if (typeList.length) {
        bms[type] = typeList.map( item => { return item.ad !== null ? item.ad.id : null; });
      }
    });


    return bms;
  }
  //endregion

  //region Lifecycle
  componentDidMount() {
    this.fetchCategoryTypes(this.props.params.category);
    this.filterByAction(this.props);

    const container = this.refs.container;

    window.addEventListener('scroll', () => {
      if (window.pageYOffset > 82 && this.refs.container) {
        container.classList.add(css.fixed);
      } else {
        container.style.width = 'auto';
        container.classList.remove(css.fixed);
      }
    });
  }
  componentWillReceiveProps(nextProps) {

    let
      newCategory = this.props.params.category !== nextProps.params.category,
      newAction   = this.props.params.action   !== nextProps.params.action,
      newPage     = this.props.params.page     !== nextProps.params.page;

    let shouldUpdate = newCategory || newAction || newPage;

    if (shouldUpdate) {
      this.fetchCategoryTypes(nextProps.params.category);
      this.filterByAction(nextProps);
    }
  }
  //endregion

  //region Render

  render() {
    return (
      <div ref="container" className={css.AdsList}>
        <div className="container">
          <div className="row">
            {
              this.props.app.mapActive ?
                this.renderMap() :
                this.renderCatalog()
            }

            <div className="col-md-3">
              <SideBar category={this.props.params.category}/>
            </div>

          </div>
        </div>
      </div>
    );
  }
  renderCatalog() {
    let category = this.props.params.category;
    const BookmarksIdList = this.buildBmIdList(this.props.bookmark.list);

    return(
      <div className="col-md-9">
        {/*{this.renderMainActions()}*/}

        <div className={css.info}>
          {/*<h4>Объявлений: <b>{this.props.ads.CurrentAdsList ? this.props.ads.CurrentAdsList.count : '0'}</b></h4>*/}
        </div>

        <div className={css.listContainer}>
          {
            this.props.ads.fetching !== 'ready'
              ? this.renderAdsLoading()
              : this.props.ads.CurrentAdsList.results.map(
                  item => <Preview
                    key={`preview_${category}_${item.id}`}
                    adType={category}
                    item={item}
                    inBookmarks={BookmarksIdList[category] ? BookmarksIdList[category].includes(item.id) : false}
                    bookmarks={this.props.bookmark.list}
                    fetchingBm={this.props.bookmark.fetching}
                    addBookmark={this.addBookmark.bind(this)}
                    removeBookmark={this.removeBookmark.bind(this)}
                    fetchAds={this.fetchAdsData.bind(this)}
                  />
                )
          }
        </div>

        <Pagination
          route={`/catalog/${this.props.params.category}/${this.props.params.action}`}
          count={this.props.ads.CurrentAdsList ? this.props.ads.CurrentAdsList.count : 0}
        />
      </div>
    );
  }
  renderMap() {
    return(
      <div className="col-md-9">
        <MainMap
          category={this.props.params.category}
          filterParams={this.props.ads.filterParams}/>
      </div>
    );
  }
  renderMainActions() {
    return(
      <div className={css.adsActions}>
        <div className="row">
          <div className="col-md-5">
            {this.renderCategorySelect()}
          </div>
        </div>
      </div>
    );
  }
  renderCategorySelect() {
    let category = this.props.params.category;

    if (category === 'cargoes') {
      return null;
    } else {
      return(
        <select onChange={this.handleCategoryTypeChange.bind(this)} name="filter_category" id="filter_category">
          <option value="">Все категории</option>
          {
            this.state.categoryTypes.map( type => {
              return <option key={`type_${type.id}`} value={type.id}>{type.name}</option>;
            })
          }
        </select>
      );
    }
  }
  renderSearchInput() {
    return(
      <div className={css.search} title="В разработке">
        <input type="text" required placeholder="Поиск по названию"/>
        <button>Искать</button>
      </div>
    );
  }
  renderAdsLoading() {
    return(
      <div>
        <AdsPreview />
      </div>
    );
  }

  //endregion
}

export default connect(mapStateToProps, mapDispatchToProps)(AdsList);
function mapStateToProps(state) {
  return {
    ads: state.ads,
    app: state.app,
    user: state.user,
    bookmark: state.bookmark
  };
}
function mapDispatchToProps(dispatch) {
  return {
    AdsActions: bindActionCreators(AdsActions, dispatch),
    AppActions: bindActionCreators(AppActions, dispatch),
    BookmarkActions: bindActionCreators(BookmarkActions, dispatch),
  };
}