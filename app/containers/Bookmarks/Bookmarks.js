import React from 'react';
import {Link} from 'react-router';

import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import * as BookmarkActions from '../../redux/actions/BookmarkActions';

import MobilePromo from '../../components/MobilePromo/MobilePromo';
import AdsPreview  from '../../components/Preview/AdsPreview/AdsPreview.js';
import Preview    from '../../components/Preview/Preview/Preview.jsx';

import css from './style.styl';


class Bookmarks extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      category: 'transport',
      bookmarks: [],
      ready: false
    };
  }

  /** Создание закладки
   * @param {string} ad_type - тип объявления
   * @param {number} ad_id - id объявления
   */
  addBookmark(ad_type, ad_id) {
    let token = localStorage.getItem('clientToken');

    if (token) this.props.BookmarkActions.addBookmark(ad_type, ad_id, token);
  }
  /** Удаление закладки
   * @param {string} ad_type - тип объявления
   * @param {number} ad_id - id объявления
   */
  removeBookmark(ad_type, ad_id) {
    let token = localStorage.getItem('clientToken');

    console.log(ad_id);

    let bmByAdId = this.props.bookmark.list.filter( item => item.ad.id === ad_id && item.ad_type === ad_type );

    this.props.BookmarkActions.removeBookmark(bmByAdId[0].id, token);
  }
  /** Переключени категории закладок
   * @param {string} category - категория закладки
   */
  setCategoryBookmarks(category) {
    let bookmarks = this.getBookmarksByCategory(category);

    if ( bookmarks.length && !this.props.bookmark.fetch ) {
      this.setState({
        category: category !== undefined ? category : 'transport',
        ready: true,
        bookmarks
      });
    }
  }
  /** Возвращает массив закладок по выбранной категории
   * @param {string} category - категория закладки
   */
  getBookmarksByCategory(category) {
    let bmArray = [];

    if(!this.props.bookmark.fetching) {
      this.props.bookmark.list.forEach( (item) => {
        if (item.ad_type === category && item.ad !== null) {
          bmArray.push(item.ad);
        }
      });
    }

    return bmArray;
  }
  /** Проверяет наличие закладок в категории */
  checkBookmarkLength(category) {
    let categories = ['transport', 'equipment', 'shop', 'cargo', 'service', 'vacancy', 'resume'];

    if (this.getBookmarksByCategory(category).length) {
      this.setCategoryBookmarks(category);
    } else {

      categories.forEach( (category) => {
        if (this.getBookmarksByCategory(category).length) {
          this.setCategoryBookmarks(category);
          return null;
        }
      });

    }
  }

  componentDidMount() {
    let localToken = localStorage.getItem('clientToken');
    this.props.BookmarkActions.fetchBookmarks( localToken );



    this.setCategoryBookmarks(this.props.params.category);
  }
  componentWillReceiveProps(nextProps) {
    this.checkBookmarkLength(nextProps.params.category);

    console.log(nextProps.bookmark.list);
  }
  render() {
    return (
      <div className={css.bookmarks}>
        <div className="container">
          <div className="row">

            <div className="col-md-9">
              {this.props.bookmark.fetching ? 'Загрузка' : this.renderBookmarksList()}
            </div>

            <div className="col-md-3">

              <div className={css.newAd}>
                <span>Добавить объявление</span>
                <img src="/img/icons/plus.svg" alt=""/>
              </div>

              <nav className={css.catList}>
                {
                  this.getBookmarksByCategory('transport').length ?
                    <Link activeClassName={css.activeLink} to="/catalog/bookmarks/transport">
                      Спецтехника <b>({this.getBookmarksByCategory('transport').length})</b>
                    </Link> : ''
                }

                {
                  this.getBookmarksByCategory('equipment').length ?
                    <Link activeClassName={css.activeLink} to="/catalog/bookmarks/equipment">
                      Оборудование <b>({this.getBookmarksByCategory('equipment').length})</b>
                    </Link> : ''
                }

                {
                  this.getBookmarksByCategory('cargo').length ?
                    <Link activeClassName={css.activeLink} to="/catalog/bookmarks/cargo">
                      Грузы и грузоперевозки <b>({this.getBookmarksByCategory('cargo').length})</b>
                    </Link> : ''
                }
                
                {
                  this.getBookmarksByCategory('shop').length ?
                    <Link activeClassName={css.activeLink} to="/catalog/bookmarks/shops">
                      Магазины и стройматериалы <b>({this.getBookmarksByCategory('shop').length})</b>
                    </Link> : ''
                }
                
                {
                  this.getBookmarksByCategory('service').length ?
                    <Link activeClassName={css.activeLink} to="/catalog/bookmarks/service">
                      Услуги <b>({this.getBookmarksByCategory('service').length})</b>
                    </Link> : ''
                }

                {
                  this.getBookmarksByCategory('vacancy').length ?
                    <Link activeClassName={css.activeLink} to="/catalog/bookmarks/vacancy">
                      Вакансии <b>({this.getBookmarksByCategory('vacancy').length})</b>
                    </Link> : ''
                }

                {
                  this.getBookmarksByCategory('resume').length ?
                    <Link activeClassName={css.activeLink} to="/catalog/bookmarks/resume">
                      Резюме <b>({this.getBookmarksByCategory('resume').length})</b>
                    </Link> : ''
                }
              </nav>

              <MobilePromo />
            </div>

          </div>
        </div>
      </div>
    );
  }


  renderBookmarksList() {
    if (this.state.ready && !this.props.bookmark.fetching) {

      if (this.state.bookmarks.length) {
        return(
          <div className={css.bookmarksList}>
            {
              this.state.bookmarks.map( item => {
                if (item === null) {
                  return null;
                } else {
                  return (
                    <Preview
                      key={`preview_${item.id}`}
                      adType={this.state.category}
                      item={item}
                      inBookmarks={true}
                      bookmarks={this.props.bookmark.list}
                      fetchingBm={this.props.bookmark.fetching}
                      addBookmark={this.addBookmark.bind( this )}
                      removeBookmark={this.removeBookmark.bind( this )}
                    />
                  );
                }
              })
            }
          </div>
        );
      } else {
        return <h3>В данной категории нет закладок</h3>;
      }

    } else {
      return <h3>В данной категории нет закладок</h3>;
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Bookmarks);
function mapStateToProps(state) {
  return {
    bookmark: state.bookmark
  };
}
function mapDispatchToProps(dispatch) {
  return {
    BookmarkActions: bindActionCreators(BookmarkActions, dispatch),
  };
}