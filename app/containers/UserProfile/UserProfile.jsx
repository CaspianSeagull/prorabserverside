import React from 'react';

import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import * as UserActions from '../../redux/actions/UserActions';

import css from './style.styl';

class UserProfile extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      userModel: {},
      uploadingPhoto: false
    };
  }

  setPhotoFetch(fetch) {
    this.setState({uploadingPhoto: fetch});
  }

  setUserModel(user) {
    this.setState( {
      userModel: user
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    let userId = this.props.user.info.id;
    let token = localStorage.getItem('clientToken');

    this.props.UserActions.updateUserInfo(userId, this.state.userModel, token);
  }

  handleFieldChange(e) {
    let value = e.target.value;
    let key   = e.target.name;

    let tempUserModel = Object.assign({}, this.state.userModel);

    tempUserModel[key] = value;

    this.setUserModel(tempUserModel);
  }

  handlePhotoSelect(e) {
    const reader = new FileReader();

    const file = e.target.files[0];

    reader.onload = () => {
      let image = document.querySelector('#userimage');
      image.src = reader.result;

      this.uploadPhoto();
    };

    reader.readAsDataURL(file);
  }


  uploadPhoto() {
    this.setPhotoFetch(true);

    let userId = this.props.user.info.id;
    let token = localStorage.getItem('clientToken');
    let fetchCallback = this.setPhotoFetch.bind(this);
    let formData = new FormData(document.querySelector('#userPhotoForm'));

    this.props.UserActions.updateUserPhoto(userId, formData, token, fetchCallback);
  }

  componentDidMount() {
    this.props.user.fetching === 'ready' ? this.setUserModel(this.props.user.info) : null;
  }

  componentWillReceiveProps(nextProps) {
    this.props.user.fetching === 'ready' ? this.setUserModel(nextProps.user.info) : null;
  }

  render() {
    return(
      <div className={css.UserProfile}>
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h2>Настройки профиля</h2>
            </div>

            <div className="col-xs-12 col-sm-3">
              {this.renderPhoto()}
            </div>

            <div className="col-xs-12 col-sm-9">
              {this.renderInfo()}
            </div>

          </div>
        </div>
      </div>
    );
  }

  renderPhoto() {
    let partial = null;

    if (this.props.user.fetching === 'ready') {
      partial = 
        <div className={css.photo}>
          <form id="userPhotoForm">
            <input
              onChange={this.handlePhotoSelect.bind(this)}
              type="file" id="userphoto" name="avatar"
            />
            <label htmlFor="userphoto"></label>
          </form>
          <img
            id="userimage"
            src={this.props.user.info.avatar}
            alt={this.props.user.info.id}
          />
          {
            this.state.uploadingPhoto ? (
              <span>
                загрузка...
              </span>
            ) : null
          }
        </div>;
    } else {
      partial = <h3>Загрузка...</h3>;
    }

    return partial;
  }

  renderInfo() {
    const User = this.props.user;

    if (User.fetching === 'ready') {
      return( 
        <div className={css.info}>
          <form name='UserInfo'>
            <label>Имя</label>
            <input type="text" onChange={this.handleFieldChange.bind(this)} name="first_name" defaultValue={User.info.first_name} placeholder='Ваше имя' required />

            <label>Фамилия</label>
            <input type="text" onChange={this.handleFieldChange.bind(this)} name="last_name" defaultValue={User.info.last_name} placeholder='Ваша фамилия' required />

            <label>E-Mail</label>
            <input type="email" onChange={this.handleFieldChange.bind(this)} name="email" defaultValue={User.info.email} placeholder='Ваш email' required />

            <label>Телефон</label>
            <input type="tel" title="Нельзя изменить номер телефона" defaultValue={User.info.phone} disabled />

            <button onClick={this.handleSubmit.bind(this)}>Сохранить</button>
          </form>
        </div>
      );
    } else {
      return <h3>Загрузка...</h3>;
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
function mapStateToProps(state) {
  return {
    user: state.user
  };
}
function mapDispatchToProps(dispatch) {
  return {
    UserActions: bindActionCreators(UserActions, dispatch)
  };
}