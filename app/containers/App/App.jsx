import React from 'react';

import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import * as AppActions      from '../../redux/actions/AppActions';
import * as UserActions     from '../../redux/actions/UserActions';
import * as BookmarkActions from '../../redux/actions/BookmarkActions';

import Geolocator       from '../../components/Geolocator/Geolocator';
import TopLine          from '../../components/TopLine/TopLine';
import Footer           from '../../components/Footer/Footer';
import MobileAppSplash  from '../../components/MobileAppSplash/MobileAppSplash';
import MainService      from '../../components/MainService/MainService';


import css from './style.styl';

/**
 * Входная точка приложения
 * 
 * @class App
 * @extends {React.Component}
 */
class App extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let clientToken = localStorage.getItem('clientToken');

    if(clientToken) {
      this.props.UserActions.fetchUserData( clientToken );
      this.props.BookmarkActions.fetchBookmarks( clientToken );
    }
  }

  render() {
    return (
      <div className={css.wrapper}>
        <MobileAppSplash />

        <div className='container'>
          <Geolocator />

          <TopLine
            authorized={this.props.user.authorized}
            user={this.props.user.info}
            userLogout={this.props.UserActions.logOut}
            city={this.props.user.city}
            setUserCity={this.props.UserActions.setUserCity}
            verifyUser={this.props.UserActions.verifyUser}
          />
        </div>


        <div className={css.mainContent}>
          {this.props.children}
        </div>

        <MainService />

        <Footer />
      </div>
    );
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);
function mapStateToProps(state) {
  return {
    app: state.app,
    user: state.user,
    bookmarks: state.bookmark
  };
}
function mapDispatchToProps(dispatch) {
  return {
    AppActions: bindActionCreators(AppActions, dispatch),
    UserActions: bindActionCreators(UserActions, dispatch),
    BookmarkActions: bindActionCreators(BookmarkActions, dispatch)
  };
}