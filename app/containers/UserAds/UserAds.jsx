import React from 'react';
import {Link} from 'react-router';

import {RusShort as RusTypes} from '../../types_local';

import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import * as BookmarkActions from '../../redux/actions/BookmarkActions';
import * as AdsActions from '../../redux/actions/AdsActions';
import * as UserActions from '../../redux/actions/UserActions';

import MobilePromo from '../../components/MobilePromo/MobilePromo';
import Preview     from '../../components/Preview/Preview/Preview.jsx';
import AdsPreview  from '../../components/Preview/AdsPreview/AdsPreview';
import Menu        from '../../components/Menu/Menu';

import css from './style.styl';


class UserAds extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      type: 'transport'
    };
  }

  fetchAds() {
    console.log(`Fetching ${this.state.type}`);
    if (this.props.user.fetching === 'ready') {
      this.props.AdsActions.setAdsListAsync(this.state.type, 1, {owner: this.props.user.info.id}, true);
    }
  }

  fetchAdsData(filterParams) {
    this.props.AdsActions.setAdsListAsync(
      this.props.params.category,
      this.state.currentPage,
      filterParams || this.props.ads.filterParams
    );
  }

  setFirstActiveType(ads_count) {
    for (let type in ads_count) {
      if (ads_count[type] > 0) {
        this.setType(type);
        break;
      }
    }
  }

  addBookmark(ad_type, ad_id) {
    let token = localStorage.getItem('clientToken');
    this.props.BookmarkActions.addBookmark(ad_type, ad_id, token);
  }
  removeBookmark(ad_type, ad_id) {
    let token = localStorage.getItem('clientToken');
    let bmByAdId = this.props.bookmark.list.filter( item => item.ad !== null && item.ad.id === ad_id && item.ad_type === ad_type );

    this.props.BookmarkActions.removeBookmark(bmByAdId[0].id, token);
  }

  buildBmIdList(bookmarks) {
    let types = ['transport', 'equipment', 'cargoes', 'shops', 'service', 'resume', 'vacancy'];
    const bms = {};

    types.forEach( (type) => {
      let typeList = bookmarks.filter(bookmark => bookmark.ad_type === type );

      if (typeList.length) {
        bms[type] = typeList.map( item => { return item.ad !== null ? item.ad.id : null; });
      }
    });


    return bms;
  }

  setType(type) {
    this.setState({type}, () => {
      this.fetchAds();
    });
  }

  componentDidMount() {
    this.setType(this.props.params.type);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params.type !== this.state.type) {
      this.setType(nextProps.params.type);
    }
  }

  render() {
    return (
      <div className={css.AdsList}>

        <Menu
          currentMenu={'info'}
          isHidden={true}
        />

        <div className="container">
          <div className="row">

            <div className="col-xs-12">
              <h2>Мои объявления</h2>
            </div>

            <div className="col-md-9">
              {this.renderAdsList()}
            </div>

            <div className="col-md-3">

              <div className={css.newAd}>
                <Link to="/new">Добавить объявление</Link>
                <img src="/img/icons/plus.svg" alt=""/>
              </div>

              {this.props.user.fetching === 'ready' ? this.renderTypeNavigation(this.props.user.info.ads_count) : null}

              <MobilePromo />
            </div>

          </div>
        </div>
      </div>
    );
  }

  renderTypeNavigation(ads) {
    let menuItems = [];

    for (let type in ads) {
      if (ads.hasOwnProperty(type) && ads[type] > 0) {
        menuItems.push(
          <Link key={type} activeClassName={css.activeLink} to={`/user/ads/${type}`}>
            {RusTypes[type]} (<b>{ads[type]}</b>)
          </Link>
        );
      }
    }

    return <nav className={css.catList}> {menuItems} </nav>;
  }


  renderAdsList() {
    let type = this.props.params.type;
    const BookmarksIdList = this.buildBmIdList(this.props.bookmark.list);

    return (
      <div className={css.listContainer}>
        {
          this.props.ads.fetching !== 'ready'
            ? this.renderAdsLoading()
            : this.props.ads.CurrentAdsList.results.map(
                item => <Preview
                  key={`preview_${type}_${item.id}`}
                  adType={type}
                  item={item}
                  inBookmarks={BookmarksIdList[type] ? BookmarksIdList[type].includes(item.id) : false}
                  bookmarks={this.props.bookmark.list}
                  fetchingBm={this.props.bookmark.fetching}
                  addBookmark={this.addBookmark.bind(this)}
                  removeBookmark={this.removeBookmark.bind(this)}
                  fetchAds={this.fetchAdsData.bind(this)}
                />
              )
        }
      </div>
    );
  }

  renderAdsLoading() {
    return(
      <div>
        <AdsPreview />
      </div>
    );
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(UserAds);
function mapStateToProps(state) {
  return {
    user: state.user,
    ads: state.ads,
    bookmark: state.bookmark
  };
}
function mapDispatchToProps(dispatch) {
  return {
    UserActions: bindActionCreators(UserActions, dispatch),
    AdsActions: bindActionCreators(AdsActions, dispatch),
    BookmarkActions: bindActionCreators(BookmarkActions, dispatch),
  };
}