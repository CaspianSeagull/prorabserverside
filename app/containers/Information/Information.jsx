import React from 'react';
import {Link} from 'react-router';
import axios from 'axios';

import Menu from '../../components/Menu/Menu';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';

import css from './style.styl';
import articleStyles from './article.css';

import {Info} from './pages';

export default class Information extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 'about'
    };
  }

  setPage(page) {
    this.setState({page}, () => {
      axios
        .get(`/pages/${this.state.page}.html`)
        .then( response => {
          this.setArticleContent(response.data);
        })
        .catch( error => {
          this.setArticleContent(error.response.statusText);
        });
    });
  }

  setArticleContent(content) {
    this.refs.content.innerHTML = content;
  }

  componentDidMount() {
    let page = this.props.params.page;

    if (page) {
      this.setPage(page);
    }
  }

  componentWillReceiveProps(nextProps) {
    let page = nextProps.params.page;

    if (page && this.state.page !== page) {
      this.setPage(page);
    }
  }

  render() {
    return (
      <div className={css.information}>

        <Menu
          currentMenu={'info'}
          isHidden={true}
        />

        <div className="container">
          <div className="row">

            <div className="col-xs-12 col-sm-9">
              {this.renderSearch()}

              {this.renderArticle()}
            </div>

            <div className="col-xs-12 col-sm-3">
              {Navigation(Info.pages)}
            </div>

          </div>
        </div>

      </div>
    );
  }

  renderArticle() {
    return(
      <div className={css.article}>
        <Breadcrumbs/>
        <div ref="content" className={css.content} />
      </div>
    );
  }

  renderSearch() {
    return (
      <div className={css.search}>
        <input type="text"/>
        <button>Искать</button>
      </div>
    );
  }
}

/** Рисуем навигацию
 *
 * @param {Array} pages - массив с объектами ссылок
 * @constructor
 *
 * @return {html} - react element
 */
const Navigation = (pages) =>
  <nav className={css.sidemenu}>
    {
      pages.map( link => {

        if (link.submenu && link.submenu.length > 1) {
          return subLink(link.route, link.title, link.submenu);
        } else {
          return navLink(link.route, link.title);
        }

      })
    }
  </nav>;

/** Рисуем пункт меню с подменю
 *
 * @param {string} route - путь к странице
 * @param {string} title - название страницы
 * @param {Array} submenu - массив сыылок для подменю
 *
 * @return {html} - react element
 */
const subLink = (route, title, submenu) =>
  <div key={`menu_${route}`} className={css.link}>
    <Link to={`/information/${route}`}><b>{title}</b></Link>
    <nav className={css.submenu}>
      {
        submenu.map( link => navLink(link.route, link.title))
      }
    </nav>
  </div>;

/** Рисуем пункт меню
 *
 * @param {string} route - путь к странице
 * @param {string} title - название страницы
 *
 * @return {html} - react element
 */
const navLink = (route, title) =>
  <div key={`menu_${route}`} className={css.link}>
    <Link activeClassName={css.active} to={`/information/${route}`}>{title}</Link>
  </div>;

