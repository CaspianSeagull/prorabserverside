export const Info = {
  pages: [
    {
      title: 'О сервисе',
      route: 'about'
    },
    {
      title: 'Обратная связь',
      route: 'feedback'
    },
    {
      title: 'Вакансии',
      route: 'job'
    },
    {
      title: 'Помощь',
      route: 'help',
      submenu: [
        {
          title: 'Частые вопросы о сервисе',
          route: 'faq'
        },
        {
          title: 'Авторизация и личный кабинет',
          route: 'profile'
        },
        {
          title: 'Размещение объявлений',
          route: 'publishing'
        },
        {
          title: 'Оплата и услуги',
          route: 'payment'
        },
        {
          title: 'Технические вопросы',
          route: 'support'
        }
      ]
    },
    {
      title: 'Разместить рекламу',
      route: 'promote'
    },
    {
      title: 'Соглашения, оферта',
      route: 'agreement'
    }
  ]
};