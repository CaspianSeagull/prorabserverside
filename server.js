const css_hash = require('./package').config.css;
const stylusCompiler = require("./config/babel-css-module.config.js");

require('babel-register')({
    "presets": ["es2015", "react"],
    "plugins": [
        [
            "css-modules-transform", {
                "extensions": [".css", ".styl"],
                "generateScopedName": css_hash,
                "preprocessCss": stylusCompiler
            }
        ]
    ]
});


const express = require('express');
const app = express();

app.use(express.static('app/public'));
app.use(require('./routes/index.jsx'));

const PORT = 3000;
app.listen(PORT, _ => console.log('Running at http://localhost:' + PORT) );
