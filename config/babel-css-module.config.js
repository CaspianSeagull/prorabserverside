const stylus = require('stylus');
const path = require('path');

module.exports = function preprocessCss(data, filename) {
    let result;

    result = stylus(data)
      .set('filename', filename)
      .render();

    return result.toString('utf8');
};