const webpack = require("webpack");

const csso = require("postcss-csso");
const discardDuplicates = require("postcss-discard-duplicates");
const autoprefixer = require("autoprefixer");

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");


const plugins = [
  new webpack.HotModuleReplacementPlugin(),
  new webpack.optimize.DedupePlugin(),
  new webpack.NoErrorsPlugin(),

  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production')
    }
  }),

  new webpack.optimize.UglifyJsPlugin({
    output: {comments: false},
    compress: {
      warnings: false
    }
  }),

  new ExtractTextPlugin("app/bundle.css")
];


const production = {  
  postcss: () => [discardDuplicates, autoprefixer, csso],
  
  plugins: plugins
};


module.exports = production;