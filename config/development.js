const webpack = require("webpack");

const csso = require("postcss-csso");
const discardDuplicates = require("postcss-discard-duplicates");
const autoprefixer = require("autoprefixer");

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");


const plugins = [
  new ExtractTextPlugin("app/bundle.css")
];


const development = {
  devtool: "eval",

  plugins: plugins
};


module.exports = development;