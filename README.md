## ServerSide вариант [Pro-Rab](http://pro-rab.ru)

### Установка

```
npm install -g webpack nodemon && npm install
```

### Команды

Выполняет сборку приложения и запускает сервер
```
npm start
```

Запускает сборку для разработки с отслеживанием изменений и запуском nodemon для обновления сервера при изменениях
```
npm run dev
```