import React from 'react';
import {Router, Route, IndexRoute, browserHistory} from 'react-router'

import MainSection from '../app/containers/MainSection/MainSection';
import Catalog     from '../app/containers/Catalog/Catalog';
import Create      from '../app/containers/Create/Create';
import ViewItem    from '../app/containers/ViewItem/ViewItem';
import AdsList     from '../app/containers/AdsList/AdsList';
import UserAds     from '../app/containers/UserAds/UserAds.jsx';
import UserProfile from '../app/containers/UserProfile/UserProfile.jsx';
import Bookmarks   from '../app/containers/Bookmarks/Bookmarks';
import Information from '../app/containers/Information/Information.jsx';
import NotFound    from '../app/containers/NotFound/NotFound';

export const routes =
  <Router history={browserHistory}>
    <Route path='/' component={require('../views/Layout.jsx')}>
      <IndexRoute name='main' component={MainSection}/>
      
      <Route name='new' path='new' component={Create}/>

      <Route name='edit' path='edit/:type/:id' edit component={Create}/>

      <Route name='information' path='information/:page' component={Information}/>

      <Route name='user_ads' path='user/ads'>
        <IndexRoute component={UserAds}/>

        <Route path=':type' component={UserAds}/>
      </Route>


      <Route name='profile' path='user/profile'>
        <IndexRoute component={UserProfile}/>

        <Route path=':type' component={UserProfile}/>
      </Route>

      <Route path='catalog' component={Catalog}>

        <Route name='Объявления' path='user/ads'>
          <IndexRoute component={UserAds}/>

          <Route path=':type' component={UserAds}/>
        </Route>
        
        <Route name='bookmarks' path='bookmarks'>
          <IndexRoute component={Bookmarks}/>

          <Route path=':category' component={Bookmarks}/>
        </Route>

        <Route name='view_ad' path='view/:category/:id' menuHidden={true} component={ViewItem} />

        <Route name='catalog_list' path=':category(/:action(/:page))' component={AdsList}/>
      </Route>

      <Route path='*' component={NotFound} />
    </Route>
  </Router>;

